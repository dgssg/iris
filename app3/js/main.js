var DEF_WASM_PATH = '/js/datasymbol-sdk.wasm';

var DEF_BARCODE_TYPE = ['Code128', 'Code39', 'EAN13', 'UPCA', 'DataMatrix', 'QRCode', 'QRCodeUnrecognized'];

var jCurrentActiveBtn = null;

window.onload = function ()
{
	DSScanner.addEventListener('onError', onError);

	DSScanner.getVideoDevices(function (devices) {
		devices.forEach(function (device) {
			console.log("device:" + device.label + '|' + device.id);
		});
		if(devices.length > 0)
			CreateScanner(devices[0]);
		else
			onError( {name: 'NotFoundError', message: ''} );
	});

	//scan barcodes click
	$('div.ScanBarcodesBtn').on('click', function() {
		ScanBarcodes(this);
	});

	//ESC and hide popup
	$(document).keyup(function(e) {
		if (e.keyCode == 27 || e.keyCode == 13) {
			HidePopup(e);
		}
	});

	//listen the mouseup to hide popup
	$(document).on('touchend mouseup', function(e) {
		/*e.stopPropagation(); // Stop event bubbling.
		e.preventDefault(); // Prevent default behaviour
		if (e.type === 'touchend') $(this).off('click'); // If event type was touch turn off clicks to prevent phantom clicks*/
		HidePopup(e);
	});

	$("div.ScanBarcodesBtn").hover(function(){
	    $(this).css("background-color", "#889");
	    }, function(){
	    $(this).css("background-color", "#404050");
	});

       	SetErrorStatus("Loading ...", "", false);
}

function HidePopup(e)
{
	var popup = $('div.popupwnd:visible');
	if(!popup || !popup.length) return;
	// if the target of the click isn't the container nor a descendant of the container
	if( e == null || (!popup.is(e.target) && popup.has(e.target).length === 0) )
	{
		DSScanner.StopScanner();

		jCurrentActiveBtn = null;

		popup.fadeOut(1000);
		//popup.hide();
		popup.css({opacity:0.1});
	}
}

function onBarcodeReady(barcodeResult)
{
	var bRed = false;
	for (var i = 0; i < barcodeResult.length; ++i) {
		if(barcodeResult[i].type == 'LinearUnrecognized' || barcodeResult[i].type == 'QRCodeUnrecognized' || barcodeResult[i].type == 'DataMatrixUnrecognized' || barcodeResult[i].type == 'PDF417Unrecognized' || barcodeResult[i].type == 'AztecUnrecognized')
		{
			bRed = true;
			continue;
		}

	        var sBarcode = DSScanner.bin2String(barcodeResult[i]);
	}

	if( bRed && barcodeResult.length == 1 )
		return;

	if( jCurrentActiveBtn != null )
	{
		var _jCurrentActiveBtn = jCurrentActiveBtn;
		var divDisc = jCurrentActiveBtn.closest('div.row').find('div.discabove').eq(0);
		divDisc.css( "visibility", "visible" );
		divDisc.addClass("swing");

		jCurrentActiveBtn.css( "background-color", "#070" );
		setTimeout( function () {
			_jCurrentActiveBtn.css( "background-color", "#404050" );
		}, 300 );
	}

	HidePopup(null);
}


function onError(err) {
	console.error(err);
	SetErrorStatus("", "", false);

	var unsupportedMsg = 'Please use any of the following browsers<br><br><table style="border-collapse:separate;border-spacing:20px;"><tr><td><b>Desktop:</b></td><td><b>Mobile Android:</b></td><td><b>Mobile iOS:</b></td></tr><tr><td>Chrome 57+<br>Firefox 52+<br>Opera 44+<br>Safari 11+<br>Edge 16+</td><td valign="top">Chrome 59+<br>Firefox 55+</td><td valign="top">Safari 11+</td></tr></table>';

	if(err.name == 'NotFoundError')
        	SetErrorStatus("No Camera", "");
	else if(err.name == 'PermissionDeniedError')
        	SetErrorStatus("Permission Denied", "");
	else if(err.name == 'NotAllowedError')
        	SetErrorStatus("Not Allowed", err.message);
	else if(err.name == 'NotCompatibleBrowser' || err.name == 'ModuleAbort' || err.name == 'CannotEnumDevices')
        	SetErrorStatus("Unsupported Browser", unsupportedMsg);
	else if(err.name == 'CannotInitLib')
        	SetErrorStatus("WASM Library Error", "Cannot initialize barcode decoder SDK");
	else
	{
        	SetErrorStatus("Error", err.message);
	}
}

function CreateScanner(device){
	var bMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	var scannerSettings = {
		scanner: {
			key: 'B053EsVoNC4TT98dgSmmLkDBGFYe4UuRrqcYiyAVwuZ++Wgzeahz4r9TczQhPy/vB1Rx6iLa/ar6NSPSgZqOj4I4UE1Ybsg/GnNcf+1uEaDOK4OmW81E/fkrAIwwYcQKAR2CjuD4TSK3Q5BQPKn+gce/8ZEZUO3WjXUk68TFHQJzpB0LW2Sw0iSezprt7jU8wpZcdDesXkaYvS4p2zIOZqGhSPjnE4yJoX8Kx1W9QY+hzNZRj3jCdBgQOVANb2boB3DSpFKE4M/utaBctP/Q6ZXVGhiipWHqBjQ9byjLLssAauz45b5xTAKJYdB+sxe/C2/NnVRabq26iIUbdlkoAA==',
			//beepData: 'https://websdk.datasymbol.com/audio/beep1.mp3',
		},
		viewport: {
			id: 'datasymbol-barcode-viewport',
			width: null,	//null means 100% width
			//width: bMobile ? null : 640,	//null means 100% width
			//height: 200,
		},
		camera: {
			resx: 640,
			resy: 480,
		},
		barcode: {
			barcodeTypes: DEF_BARCODE_TYPE,
		},
	};

	if(bMobile)
	{
		scannerSettings.camera.facingMode = 'environment';
	}
	else
	{
		scannerSettings.camera.id = device ? device.id : null;
		scannerSettings.camera.label = device ? device.label : null;
	}

	//DSScanner.addEventListener('onError', onError);

	DSScanner.addEventListener('onBarcode', onBarcodeReady);

	DSScanner.addEventListener('onScannerReady', function () {
		console.log('HTML onScannerReady');
		SetErrorStatus("", "", false);
		//DSScanner.StartScanner();
	});

	DSScanner.Create(scannerSettings);
}

function SetErrorStatus(errTitle, errMsg, bShowImg=true) {
	var errMessage = "<center>";
	if(bShowImg)
		errMessage += "<img src='/images/warning_64.png' border=0></br>";
	errMessage += "<span class='v2'>"+errTitle+"</span></center></br>";
	errMessage += errMsg;

	var statusElement = document.getElementById('status');
	statusElement.innerHTML = errMessage;
}

function ShowPopup(btnEl)
{
	jCurrentActiveBtn = $(btnEl);
	var buttonPos = jCurrentActiveBtn.offset();

	var jDivPopupWnd = $('#ScanBarcodesDiv');

	//attach popup to the button div
	jDivPopupWnd.detach();
	jCurrentActiveBtn.append(jDivPopupWnd);

	var _top = jCurrentActiveBtn.outerHeight() + 3;
	jDivPopupWnd.css({left: -1, top: _top-30, display:'inline-block'});
	jDivPopupWnd.outerWidth( jCurrentActiveBtn.outerWidth() );
	jDivPopupWnd.animate({top: _top, opacity: 1}, 300);
}

function ScanBarcodes(el)
{
	if( $("div#ScanBarcodesDiv:visible").length != 0 )
		return;

	ShowPopup(el);

	//if scanner ready, start scanning
	if( DSScanner.IsScannerReady() )
	{
		DSScanner.StartScanner();
	}
	//try latter (3s)
	else
	{
		setTimeout( function () {
			DSScanner.StartScanner();
		}, 3000 );
	}
}
