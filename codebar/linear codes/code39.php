<div>
    <div style="padding:10px; font-size:15px; font-family:Arial, Helvetica; text-align:center; display:inline-block;">
        <a href="https://www.cognex.com" title="Cognex Corporation" style="display:block">
            <img src="https://www.cognex.com/gfx/site/pic-global-header-logo-cognex.png" alt="Barcode Software by Cognex Corporation"/>
        </a>
        <a href="https://www.cognex.com" title="Cognex Corporation">Cognex Corporation</a>
    </div>
    <div style="display:inline;">
        <img src="https://www.cognex.com/api/Sitecore/Barcode/Get?data=ABC-1234&code=BCL_CODE39&width=300&imageType=JPG&foreColor=%23000000&backColor=%23FFFFFF&rotation=RotateNoneFlipNone" alt="MK Sistemas Biomedicos" width="300" />
    </div>
</div>
