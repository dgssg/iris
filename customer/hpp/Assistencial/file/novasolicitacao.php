<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: login.php");
	}

	// Adiciona a Função display_campo($nome_campo, $tipo_campo)
	//require_once "personalizacao_display.php";
		$usuariologado=$_SESSION['usuario'];
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon"href="../../framework/images/favicon.ico" type="image/ico" />
		<title>IRIS -| Assistencial</title>



		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="../../framework/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="../../framework/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="../../framework/assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="../../framework/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../../framework/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="../../framework/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../../framework/assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="../../framework/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="../../framework/assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="../../framework/assets/js/html5shiv.min.js"></script>
		<script src="../../framework/assets/js/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="css/content-style.css"/>
		<link rel="stylesheet" href="css/mricode.pagination.css"/>

		<!-- BEGIN GLOBAL MANDATORY STYLES -->


		<!-- END GLOBAL MANDATORY STYLES -->

		<!-- BEGIN PAGE LEVEL STYLES -->

		<link rel="stylesheet" type="text/css" href="media/css/bootstrap-fileupload.css" />

		<link rel="stylesheet" type="text/css" href="media/css/jquery.gritter.css" />

		<link rel="stylesheet" type="text/css" href="media/css/chosen.css" />

		<link rel="stylesheet" type="text/css" href="media/css/select2_metro.css" />

		<link rel="stylesheet" type="text/css" href="media/css/jquery.tagsinput.css" />

		<link rel="stylesheet" type="text/css" href="media/css/clockface.css" />

		<link rel="stylesheet" type="text/css" href="media/css/bootstrap-wysihtml5.css" />

		<link rel="stylesheet" type="text/css" href="media/css/datepicker.css" />

		<link rel="stylesheet" type="text/css" href="media/css/timepicker.css" />

		<link rel="stylesheet" type="text/css" href="media/css/colorpicker.css" />

		<link rel="stylesheet" type="text/css" href="media/css/bootstrap-toggle-buttons.css" />

		<link rel="stylesheet" type="text/css" href="media/css/daterangepicker.css" />

		<link rel="stylesheet" type="text/css" href="media/css/datetimepicker.css" />

		<link rel="stylesheet" type="text/css" href="media/css/multi-select-metro.css" />

		<link href="media/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

		<!-- END PAGE LEVEL STYLES -->
		<!-- PNotify -->
	<link href="vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	<!-- bootstrap-progressbar -->
		<link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="index.php" class="navbar-brand">
						<small>
							<i class="fa fa-eye"></i>
							IRIS |- Setor Assistencial  <?php printf($setor) ?>
						</small>
					</a>
				</div>

		<!--		<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="grey dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-tasks"></i>
								<span class="badge badge-grey">4</span>
							</a>

				<!--			<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-check"></i>
									4 Tasks to complete
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar">
										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">Software Update</span>
													<span class="pull-right">65%</span>
												</div>

												<div class="progress progress-mini">
													<div style="width:65%" class="progress-bar"></div>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">Hardware Upgrade</span>
													<span class="pull-right">35%</span>
												</div>

												<div class="progress progress-mini">
													<div style="width:35%" class="progress-bar progress-bar-danger"></div>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">Unit Testing</span>
													<span class="pull-right">15%</span>
												</div>

												<div class="progress progress-mini">
													<div style="width:15%" class="progress-bar progress-bar-warning"></div>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">Bug Fixes</span>
													<span class="pull-right">90%</span>
												</div>

												<div class="progress progress-mini progress-striped active">
													<div style="width:90%" class="progress-bar progress-bar-success"></div>
												</div>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="#">
										See tasks with details
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="purple dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<span class="badge badge-important">8</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									8 Notifications
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-pink fa fa-comment"></i>
														New Comments
													</span>
													<span class="pull-right badge badge-info">+12</span>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<i class="btn btn-xs btn-primary fa fa-user"></i>
												Bob just signed up as an editor ...
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-success fa fa-shopping-cart"></i>
														New Orders
													</span>
													<span class="pull-right badge badge-success">+8</span>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-info fa fa-twitter"></i>
														Followers
													</span>
													<span class="pull-right badge badge-info">+11</span>
												</div>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="#">
										See all notifications
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="green dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">5</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-envelope-o"></i>
									5 Messages
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar">
										<li>
											<a href="#" class="clearfix">
												<img src="../../framework/assets/images/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">Alex:</span>
														Ciao sociis natoque penatibus et auctor ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>a moment ago</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="../../framework/assets/images/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">Susan:</span>
														Vestibulum id ligula porta felis euismod ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>20 minutes ago</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="../../framework/assets/images/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">Bob:</span>
														Nullam quis risus eget urna mollis ornare ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>3:15 pm</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="../../framework/assets/images/avatars/avatar2.png" class="msg-photo" alt="Kate's Avatar" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">Kate:</span>
														Ciao sociis natoque eget urna mollis ornare ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>1:33 pm</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="../../framework/assets/images/avatars/avatar5.png" class="msg-photo" alt="Fred's Avatar" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">Fred:</span>
														Vestibulum id penatibus et auctor  ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>10:09 am</span>
													</span>
												</span>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="inbox.php">
										See all messages
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="../../framework/assets/images/avatars/user.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									Jason
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										Settings
									</a>
								</li>

								<li>
									<a href="profile.php">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="#">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>-->
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
					<a href="read_inventory.php">
				<i class="ace-icon fa fa-barcode"></i>
			</button>
		</a>
			<button class="btn btn-info">
					<a href="read_validation.php">
				<i class="ace-icon fa fa-check-square-o"></i>
			</button>
		</a>

			<button class="btn btn-warning">
					<a href="read_catalog.php">
				<i class="ace-icon fa fa-list-alt"></i>
			</button>
		</a>


			<button class="btn btn-danger">
					<a href="read_tag.php">
				<i class="ace-icon glyphicon glyphicon-tag"></i>
			</button>
		</a>
		</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->
			 <button class="btn btn-white btn-info btn-bold">
        <a href="profile.php">
												<i class="ace-icon fa fa-user bigger-140 blue"></i>
											
											</button> </a> <?php printf($usuariologado) ?>  </br>

Sessão: <p class="glyphicon glyphicon-time" id="countdown"></p>  </br>
Data: <script> document.write(new Date().toLocaleDateString()); </script> 

				<ul class="nav nav-list">
					<li class="">
						<a href="index.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

				<!--	<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								UI &amp; Elements
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="#" class="dropdown-toggle">
									<i class="menu-icon fa fa-caret-right"></i>

									Layouts
									<b class="arrow fa fa-angle-down"></b>
								</a>

								<b class="arrow"></b>

								<ul class="submenu">
									<li class="">
										<a href="top-menu.php">
											<i class="menu-icon fa fa-caret-right"></i>
											Top Menu
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="two-menu-1.php">
											<i class="menu-icon fa fa-caret-right"></i>
											Two Menus 1
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="two-menu-2.php">
											<i class="menu-icon fa fa-caret-right"></i>
											Two Menus 2
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="mobile-menu-1.php">
											<i class="menu-icon fa fa-caret-right"></i>
											Default Mobile Menu
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="mobile-menu-2.php">
											<i class="menu-icon fa fa-caret-right"></i>
											Mobile Menu 2
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="mobile-menu-3.php">
											<i class="menu-icon fa fa-caret-right"></i>
											Mobile Menu 3
										</a>

										<b class="arrow"></b>
									</li>
								</ul>
							</li>

							<li class="">
								<a href="typography.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Typography
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="elements.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Elements
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="buttons.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Buttons &amp; Icons
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="content-slider.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Content Sliders
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="treeview.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Treeview
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="jquery-ui.php">
									<i class="menu-icon fa fa-caret-right"></i>
									jQuery UI
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="nestable-list.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Nestable Lists
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="#" class="dropdown-toggle">
									<i class="menu-icon fa fa-caret-right"></i>

									Three Level Menu
									<b class="arrow fa fa-angle-down"></b>
								</a>

								<b class="arrow"></b>

								<ul class="submenu">
									<li class="">
										<a href="#">
											<i class="menu-icon fa fa-leaf green"></i>
											Item #1
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="#" class="dropdown-toggle">
											<i class="menu-icon fa fa-pencil orange"></i>

											4th level
											<b class="arrow fa fa-angle-down"></b>
										</a>

										<b class="arrow"></b>

										<ul class="submenu">
											<li class="">
												<a href="#">
													<i class="menu-icon fa fa-plus purple"></i>
													Add Product
												</a>

												<b class="arrow"></b>
											</li>

											<li class="">
												<a href="#">
													<i class="menu-icon fa fa-eye pink"></i>
													View Products
												</a>

												<b class="arrow"></b>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
					</li> -->

					<li class="">


			<!--		<	<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="tables.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Simple &amp; Dynamic
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="jqgrid.php">
									<i class="menu-icon fa fa-caret-right"></i>
									jqGrid plugin
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li> -->

					<li class="">
						<a href="solicitar.php" >
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> Solicitação </span>


						</a>


								<b class="arrow"></b>
							</li>

					</li>
<li class="">
						<a href="arsenal.php" >
							<i class="menu-icon fa fa-stack-overflow"></i>
							<span class="menu-text"> Arsenal </span>


						</a>


								<b class="arrow"></b>
							</li>

					</li>
<li class="">
						<a href="solicitar.php" >
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Profile </span>


						</a>


								<b class="arrow"></b>
							</li>

					</li>

				<!--		<li class="">
							<a href="faq.php">
								<i class="menu-icon fa fa-info"></i>
								FAQ
							</a>

							<b class="arrow"></b>
						</li> -->

					<li class="">
						<a href="logout.php">
							<i class="menu-icon glyphicon  glyphicon-off"></i>

							<span class="menu-text"> Logout </span>
						</a>

						<b class="arrow"></b>
					</li>
						</ul>
<!--
					<li class="">
						<a href="widgets.php">
							<i class="menu-icon fa fa-list-alt"></i>
							<span class="menu-text"> Widgets </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="calendar.php">
							<i class="menu-icon fa fa-calendar"></i>

							<span class="menu-text">
								Calendar

								<span class="badge badge-transparent tooltip-error" title="2 Important Events">
									<i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>
								</span>
							</span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="gallery.php">
							<i class="menu-icon fa fa-picture-o"></i>
							<span class="menu-text"> Gallery </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-tag"></i>
							<span class="menu-text"> More Pages </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="profile.php">
									<i class="menu-icon fa fa-caret-right"></i>
									User Profile
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="inbox.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Inbox
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="pricing.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Pricing Tables
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="invoice.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Invoice
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="timeline.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Timeline
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="search.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Search Results
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="email.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Email Templates
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="login.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Login &amp; Register
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li class="active open">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-file-o"></i>

							<span class="menu-text">
								Other Pages

								<span class="badge badge-primary">5</span>
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b> -->


<!--
							<li class="">
								<a href="error-404.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Error 404
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="error-500.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Error 500
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="grid.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Grid
								</a>

								<b class="arrow"></b>
							</li>

							<li class="active">
								<a href="blank.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Blank Page
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li> -->
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Dashboard</a>
							</li>

						</ul><!-- /.breadcrumb -->

				<!--		<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="container">
								  <div class="row">
								    <div class="col-md-12">
								      <div class="panel-body" style="padding: 15px 0;">
								        <ul class="nav nav-tabs">
								          <li><a href="solicitar.php">Lista de Solicitações</a></li>
								          <li  class="active"><a href="novasolicitacao.php">Solicitar</a></li>
								        </ul>
								      </div>
								      <br>
								      <div class="panel-body">
								        <form class="form-horizontal">
													<div class="row-fluid">

														<div class="span12">

															<!-- BEGIN PORTLET-->





																	<div class="caption"><i class="icon-reorder"></i>Solicitação de Material</div>

																	<div class="tools">

																		<a href="javascript:;" class="collapse"></a>

																		<a href="#portlet-config" data-toggle="modal" class="config"></a>

																		<a href="javascript:;" class="reload"></a>

																		<a href="javascript:;" class="remove"></a>

																	</div>

																</div>

																<div class="portlet-body form">

																	<!-- BEGIN FORM-->

																		<form class="form-horizontal" role="form" action="php/novasolicitacao.php" method="post" >


																		<div class="control-group">

																			<label class="control-label">Lista Material</label>

																			<div class="controls">

																				<select multiple="multiple" id="my_multi_select2" name="my_multi_select2[] ">

																					<optgroup label="Artigos Criticos">

																						<option value="1">Instrumental</option>

																						<option value="2">Cateter</option>

																						<option value="3">Implamtes</option>

																						

																					</optgroup>

																					<optgroup label="Artigos Semi-Críticos">

																						<option>Circuito Respiratorio</option>

																						<option>Endoscopio</option>

																						<option>Green Bay Packers</option>

																						

																					</optgroup>

																					<optgroup label="Artigos Semi-Críticos">

																						<option>Comadres</option>

																						<option>Papagaio</option>
																					</optgroup>
																						
																				</select>

																			</div>

																		</div>

																	

																	<!-- END FORM-->

																</div>

															</div>

															<!-- END PORTLET-->


													<hr/>

								          <div class="form-group">
								           	<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"></label>
														<div class="col-xs-2 col-sm-2">
																 <div class="input-group">
																	 <span class="input-group-addon">
																		 <i class="ace-icon fa fa-user"></i>
								              <select class="form-control" name="exchange">
								                <option value="UTI">UTI</option>
																<option value="PA">PA</option>
																<option value="PS">PS</option>
																<option value="CDI">CDI</option>
																<option value="UDT">UDT</option>
																<option value="PA1">Posto 1</option>
																<option value="PA2">Posto 2</option>
																<option value="PA3">Posto 3</option>
																<option value="PA4">Posto 4</option>
																<option value="PA5">Posto 5</option>
																<option value="PA6">Posto 6</option>
																<option value="PA7">Posto 7</option>
																<option value="PA8">Posto 8</option>
								              </select>
								            </div>
								          </div>
												</div>



													<div class="center">
														<button type="button" class="btn btn-primary" id="bootbox-confirm"
														onclick="new PNotify({
																			title: 'Cancelado',
																			text: 'Registro Cancelado',
																			styling: 'bootstrap3'
																	});" >Cancelar</button>
														<button type="reset" class="btn btn-primary"id="bootbox-confirm" onclick="new PNotify({
																			title: 'Limpado',
																			text: 'Todos os Campos Limpos',
																			type: 'info',
																			styling: 'bootstrap3'
																	});"  >Limpar</button>
														<input type="submit" class="btn btn-primary"id="bootbox-confirm" onclick="new PNotify({
																			title: 'Registrado',
																			text: 'Informações registrada!',
																			type: 'success',
																			styling: 'bootstrap3'
																	});" />
																</form>


													</div>
								      </div>
								      <hr>
								    </div>
								  </div>
								</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">IRIS</span>
							CV Healthcare &copy; 2020
						</span>

						&nbsp; &nbsp;
					<!--	<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span> -->
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="../../framework/assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="../../framework/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="../../framework/assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="../../framework/assets/js/ace-elements.min.js"></script>
		<script src="../../framework/assets/js/ace.min.js"></script>
		<script src="../../framework/js/plugins/jquery/jquery.js"></script>
		<script src="../../framework/js/bootstrap.min.js"></script>
		<script src="../../framework/js/plugins/pagination/mricode.pagination.js"></script>
		<script src="../../framework/js/common/common.js"></script>
		<script type="text/javascript"></script>
		<!-- inline scripts related to this page -->
		<!-- BEGIN CORE PLUGINS -->
		<!-- PNotify -->
	 <script src="vendors/pnotify/dist/pnotify.js"></script>
	 <script src="vendors/pnotify/dist/pnotify.buttons.js"></script>
	 <script src="vendors/pnotify/dist/pnotify.nonblock.js"></script>
		<script src="media/js/jquery-1.10.1.min.js" type="text/javascript"></script>

		<script src="media/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

		<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

		<script src="media/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>

		<script src="media/js/bootstrap.min.js" type="text/javascript"></script>

		<!--[if lt IE 9]>

		<script src="media/js/excanvas.min.js"></script>

		<script src="media/js/respond.min.js"></script>

		<![endif]-->

		<script src="media/js/jquery.slimscroll.min.js" type="text/javascript"></script>

		<script src="media/js/jquery.blockui.min.js" type="text/javascript"></script>

		<script src="media/js/jquery.cookie.min.js" type="text/javascript"></script>

		<script src="media/js/jquery.uniform.min.js" type="text/javascript" ></script>

		<!-- END CORE PLUGINS -->

		<!-- BEGIN PAGE LEVEL PLUGINS -->

		<script type="text/javascript" src="media/js/ckeditor.js"></script>

		<script type="text/javascript" src="media/js/bootstrap-fileupload.js"></script>

		<script type="text/javascript" src="media/js/chosen.jquery.min.js"></script>

		<script type="text/javascript" src="media/js/select2.min.js"></script>

		<script type="text/javascript" src="media/js/wysihtml5-0.3.0.js"></script>

		<script type="text/javascript" src="media/js/bootstrap-wysihtml5.js"></script>

		<script type="text/javascript" src="media/js/jquery.tagsinput.min.js"></script>

		<script type="text/javascript" src="media/js/jquery.toggle.buttons.js"></script>

		<script type="text/javascript" src="media/js/bootstrap-datepicker.js"></script>

		<script type="text/javascript" src="media/js/bootstrap-datetimepicker.js"></script>

		<script type="text/javascript" src="media/js/clockface.js"></script>

		<script type="text/javascript" src="media/js/date.js"></script>

		<script type="text/javascript" src="media/js/daterangepicker.js"></script>

		<script type="text/javascript" src="media/js/bootstrap-colorpicker.js"></script>

		<script type="text/javascript" src="media/js/bootstrap-timepicker.js"></script>

		<script type="text/javascript" src="media/js/jquery.inputmask.bundle.min.js"></script>

		<script type="text/javascript" src="media/js/jquery.input-ip-address-control-1.0.min.js"></script>

		<script type="text/javascript" src="media/js/jquery.multi-select.js"></script>

		<script src="media/js/bootstrap-modal.js" type="text/javascript" ></script>

		<script src="media/js/bootstrap-modalmanager.js" type="text/javascript" ></script>

		<!-- END PAGE LEVEL PLUGINS -->

		<!-- BEGIN PAGE LEVEL SCRIPTS -->

		<script src="media/js/app.js"></script>

		<script src="media/js/form-components.js"></script>

		<!-- END PAGE LEVEL SCRIPTS -->

		<script>

			jQuery(document).ready(function() {

				 // initiate layout and plugins

				 App.init();

				 FormComponents.init();

			});

		</script>

		<!-- END JAVASCRIPTS -->

	<script type="text/javascript">  var _gaq = _gaq || [];  _gaq.push(['_setAccount', 'UA-37564768-1']);  _gaq.push(['_setDomainName', 'keenthemes.com']);  _gaq.push(['_setAllowLinker', true]);  _gaq.push(['_trackPageview']);  (function() {    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);  })();</script></body>

	<!-- END BODY -->

	</body>
<script type="text/javascript">

     // Total seconds to wait
     var seconds = 1080;

     function countdown() {
         seconds = seconds - 1;
         if (seconds < 0) {
             // Chnage your redirection link here
             window.location = "http://www.iris.mksistemasbiomedicos.com.br";
         } else {
             // Update remaining seconds
             document.getElementById("countdown").innerHTML = seconds;
             // Count down using javascript
             window.setTimeout("countdown()", 1000);
         }
     }

     // Run countdown function
     countdown();

 </script>
</html>
