<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<div class="col-xs-6 col-sm-4">
    <div class="widget-box">
        <div class="widget-header">
            <h4 class="widget-title">Cadastro SubGrupo</h4>
            <span class="widget-toolbar">
                <a href="#" data-action="settings">
                    <i class="ace-icon fa fa-cog"></i>
                </a>
                <a href="#" data-action="reload">
                    <i class="ace-icon fa fa-refresh"></i>
                </a>
                <a href="#" data-action="collapse">
                    <i class="ace-icon fa fa-chevron-up"></i>
                </a>
                <a href="#" data-action="close">
                    <i class="ace-icon fa fa-times"></i>
                </a>
            </span>
        </div>
        <div class="clearfix"></div>
        <form class="dropzone" action="backend/register-instrumental-subgroup-registration-dropzone-backend.php"
            method="post">
        </form>
        <br>
       
        <div class="ln_solid"></div>
        <form class="form-horizontal" action="backend/register-instrumental-subgroup-registration-backend.php"
            onsubmit="return showConfirmationDialog(event)" method="POST">
            <div class="widget-body">
                <div class="widget-main">
                    <div>
                        <label for="form-field-select-3">
                            Grupo
                        </label>
                        <div class="input-group">
                        <select class="chosen-select form-control" name="grupo" id="form-field-select-3" data-placeholder="Selecione um grupo">
<option>Selecione um Grupo</option>
</select>


                        </div>
                    </div>
                    <hr />
                    <div>
                        <label for="nome">
                            Nome
                        </label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="nome" id="nome" />
                        </div>
                    </div>
                    <hr />
                    <div>
                        <label for="codigo">
                            Codigo
                        </label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="codigo" id="codigo" />
                        </div>
                    </div>
                    <hr />
                    <div>
                        <label for="codigo">
                            Fabricante
                        </label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="fabricante" id="fabricante" />
                        </div>
                    </div>
                    <hr />
                    <div>
                        <label for="codigo">
                            Modelo
                        </label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="modelo" id="modelo" />
                        </div>
                    </div>
                    <hr />
                    <div>
                        <label for="codigo">
                            Anvisa
                        </label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="anvisa" id="anvisa" />
                        </div>
                    </div>
                    <hr />


                    <div>
                        <button class="btn btn-primary" type="submit">Salvar</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

<script>
function showConfirmationDialog(event) {
    event.preventDefault(); // previne o envio do formulário
    Swal.fire({
        title: 'Deseja salvar as alterações?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Salvar',
        denyButtonText: 'Não salve',
    }).then((result) => {
        if (result.isConfirmed) {
            event.target.submit(); // Envia o formulário
        } else if (result.isDenied) {
            Swal.fire('As alterações não são salvas', '', 'info');
        }
    });
}
</script>
<script>
    $(document).ready(function() {
  // faz a chamada AJAX para buscar as opções do grupo
  $.ajax({
    url: 'search/search-instrumental-group.php',
    method: 'GET',
    dataType: 'json'
  }).done(function(response) {
    // se a chamada for bem-sucedida, adiciona as opções ao select
    var select = $('#form-field-select-3');
    $.each(response, function(index, group) {
      select.append('<option value="' + group.id + '">' + group.nome + ' - ' + group.codigo + '</option>');
    });
    // ativa o plugin chosen para tornar o select com filtro
    select.chosen();
  }).fail(function(jqXHR, textStatus, errorThrown) {
    // se a chamada falhar, mostra uma mensagem de erro
    console.log('Erro ao buscar opções do grupo: ' + textStatus + ' - ' + errorThrown);
  });
});

if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
                };
</script>




</div>
</div>