	<!-- PAGE CONTENT BEGINS -->
<div class="row">
									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-dark">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Energia Eletrica</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">
														<li>
															<i class="ace-icon fa fa-check green"></i>
															10 GB Disk Space
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															200 GB Bandwidth
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															100 Email Accounts
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															10 MySQL Databases
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															$10 Ad Credit
														</li>

														<li>
															<i class="ace-icon fa fa-times red"></i>
															Free Domain
														</li>
													</ul>

													<hr />
													<div class="price">
														$5
														<small>/month</small>
													</div>
												</div>

												<div>
													<a href="#" class="btn btn-block btn-inverse">
														<i class="ace-icon fa fa-shopping-cart bigger-110"></i>
														<span>Print</span>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-orange">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Agua</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">
														<li>
															<i class="ace-icon fa fa-check green"></i>
															50 GB Disk Space
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															1 TB Bandwidth
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															1000 Email Accounts
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															100 MySQL Databases
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															$25 Ad Credit
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															Free Domain
														</li>
													</ul>

													<hr />
													<div class="price">
														$10
														<small>/month</small>
													</div>
												</div>

												<div>
													<a href="#" class="btn btn-block btn-warning">
														<i class="ace-icon fa fa-shopping-cart bigger-110"></i>
														<span>Print</span>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-blue">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Integrador</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">
														<li>
															<i class="ace-icon fa fa-check green"></i>
															200 GB Disk Space
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															Unlimited Bandwidth
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															1000 Email Accounts
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															200 MySQL Databases
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															$25 Ad Credit
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															Free Domain
														</li>
													</ul>

													<hr />
													<div class="price">
														$15
														<small>/month</small>
													</div>
												</div>

												<div>
													<a href="#" class="btn btn-block btn-primary">
														<i class="ace-icon fa fa-shopping-cart bigger-110"></i>
														<span>Print</span>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-green">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Produção</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">
														<li>
															<i class="ace-icon fa fa-check green"></i>
															Unlimited Space
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															Unlimited Bandwidth
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															Unlimited Email Accounts
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															Unlimited MySQL Databases
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															$50 Ad Credit
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															2 Free Domains
														</li>
													</ul>

													<hr />
													<div class="price">
														$25
														<small>/month</small>
													</div>
												</div>

												<div>
													<a href="#" class="btn btn-block btn-success">
														<i class="ace-icon fa fa-shopping-cart bigger-110"></i>
														<span>Print</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="space-24"></div>
								<h3 class="header smaller red">Small Style</h3>

								<div class="row">
									<div class="col-xs-4 col-sm-3 pricing-span-header">
										<div class="widget-box transparent">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Package Name</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main no-padding">
													<ul class="list-unstyled list-striped pricing-table-header">
														<li>Disk Space </li>
														<li>Bandwidth </li>
														<li>Email Accounts </li>
														<li>MySQL Databases </li>
														<li>Ad Credit </li>
														<li>Free Domain </li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-8 col-sm-9 pricing-span-body">
										<div class="pricing-span">
											<div class="widget-box pricing-box-small widget-color-red3">
												<div class="widget-header">
													<h5 class="widget-title bigger lighter">Instrumental</h5>
												</div>

												<div class="widget-body">
													<div class="widget-main no-padding">
														<ul class="list-unstyled list-striped pricing-table">
															<li> 10 GB </li>
															<li> 200 GB </li>
															<li> 100 </li>
															<li> 10 </li>
															<li> $10 </li>

															<li>
																<i class="ace-icon fa fa-times red"></i>
															</li>
														</ul>

														<div class="price">
															<span class="label label-lg label-inverse arrowed-in arrowed-in-right">
																$5
																<small>/month</small>
															</span>
														</div>
													</div>

													<div>
														<a href="#" class="btn btn-block btn-sm btn-danger">
															<span>Print</span>
														</a>
													</div>
												</div>
											</div>
										</div>

										<div class="pricing-span">
											<div class="widget-box pricing-box-small widget-color-orange">
												<div class="widget-header">
													<h5 class="widget-title bigger lighter">Material</h5>
												</div>

												<div class="widget-body">
													<div class="widget-main no-padding">
														<ul class="list-unstyled list-striped pricing-table">
															<li> 50 GB </li>
															<li> 1 TB </li>
															<li> 1000 </li>
															<li> 100 </li>
															<li> $25 </li>

															<li>
																<i class="ace-icon fa fa-check green"></i>
																1
															</li>
														</ul>

														<div class="price">
															<span class="label label-lg label-inverse arrowed-in arrowed-in-right">
																$10
																<small>/month</small>
															</span>
														</div>
													</div>

													<div>
														<a href="#" class="btn btn-block btn-sm btn-warning">
															<span>Print</span>
														</a>
													</div>
												</div>
											</div>
										</div>

										<div class="pricing-span">
											<div class="widget-box pricing-box-small widget-color-blue">
												<div class="widget-header">
													<h5 class="widget-title bigger lighter">Equipamento</h5>
												</div>

												<div class="widget-body">
													<div class="widget-main no-padding">
														<ul class="list-unstyled list-striped pricing-table">
															<li> 200 GB </li>
															<li> Unlimited </li>
															<li> 1000 </li>
															<li> 200 </li>
															<li> $25 </li>

															<li>
																<i class="ace-icon fa fa-check green"></i>
																1
															</li>
														</ul>

														<div class="price">
															<span class="label label-lg label-inverse arrowed-in arrowed-in-right">
																$15
																<small>/month</small>
															</span>
														</div>
													</div>

													<div>
														<a href="#" class="btn btn-block btn-sm btn-primary">
															<span>Print</span>
														</a>
													</div>
												</div>
											</div>
										</div>

									

								
									</div>
								</div><!-- PAGE CONTENT ENDS -->
									<div class="x_panel">
					<div class="x_title">
						<h2></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

					<object  style="width:100%; height:500px"   type="text/html"  data="custos/analitico.php" > </object>

					</div>
				</div>
			</div>
				<div class="col-md-6 col-sm-6  ">
				<div class="x_panel">
					<div class="x_title">
						<h2></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

					<object  style="width:100%; height:400px"   type="text/html"  data="custos/analitico2.php" > </object>

					</div>
				</div>
			</div>
								<!-- PAGE CONTENT ENDS -->