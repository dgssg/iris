	<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="alert alert-info">
									<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
									Elabore o check list para Impressão
								</div>

								<div id="top-menu" class="modal aside" data-fixed="true" data-placement="top" data-background="true" data-backdrop="invisible" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body container">
												<div class="row">
													<div class="col-sm-5 col-sm-offset-1 white">
														<h3 class="lighter">Bootstrap Grid &amp; Elements</h3>
														With invisible backdrop
													</div>

													<div class="col-sm-5 text-center line-height-2">
														<a class="btn btn-app btn-Default no-radius" href="#">
															<i class="ace-icon fa fa-pencil-square-o bigger-230"></i>
															Default
															<span class="label label-light arrowed-in-right badge-left">11</span>
														</a>

														&nbsp; &nbsp;
														<a class="btn btn-info btn-app no-radius" href="#">
															<i class="ace-icon fa fa-cog bigger-230"></i>
															Mailbox
															<span class="label label-danger arrowed-in">6+</span>
														</a>

														&nbsp; &nbsp;
														<a class="btn btn-app btn-light no-radius" href="window.print();">
															<i class="ace-icon fa fa-print bigger-230"></i>
															Print
														</a>
													</div>
												</div>
											</div>
										</div><!-- /.modal-content -->

										<button class="btn btn-inverse btn-app btn-xs ace-settings-btn aside-trigger" data-target="#top-menu" data-toggle="modal" type="button">
											<i data-icon1="fa-chevron-down" data-icon2="fa-chevron-up" class="ace-icon fa fa-chevron-down bigger-110 icon-only"></i>
										</button>
									</div><!-- /.modal-dialog -->
								</div>

								<div id="bottom-menu" class="modal aside" data-fixed="true" data-placement="bottom" data-background="true" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body container">
												<div class="row">
													<div class="col-sm-5 col-sm-offset-1 white">
														<h3 class="lighter">Bootstrap Grid &amp; Elements</h3>
														With dark modal backdrop
													</div>
												</div>
											</div>
										</div><!-- /.modal-content -->

										<button class="btn btn-yellow btn-app btn-xs ace-settings-btn aside-trigger" data-target="#bottom-menu" data-toggle="modal" type="button">
											<i data-icon2="fa-chevron-down" data-icon1="fa-chevron-up" class="ace-icon fa fa-chevron-up bigger-110 icon-only"></i>
										</button>
									</div><!-- /.modal-dialog -->
								</div>

								<div id="right-menu" class="modal aside" data-body-scroll="false" data-offset="true" data-placement="right" data-fixed="true" data-backdrop="false" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Based on Modal boxes
												</div>
											</div>

											<div class="modal-body">
												<h3 class="lighter">Elaboração de check list</h3>

												<br />
												With no modal backdrop
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												1
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												2
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												<br />
												3
											</div>
										</div><!-- /.modal-content -->

										<button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#right-menu" data-toggle="modal" type="button">
											<i data-icon1="fa-plus" data-icon2="fa-minus" class="ace-icon fa fa-plus bigger-110 icon-only"></i>
										</button>
									</div><!-- /.modal-dialog -->
								</div>



								<div class="widget purple">
					 				 <div class="widget-title">
					 						 <h4><i class="icon-reorder"></i> Ckeck list </h4>
					 						<span class="tools">
					 								<a href="javascript:;" class="icon-chevron-down"></a>
					 								<a href="javascript:;" class="icon-remove"></a>
					 						</span>
					 				 </div>
					 				 <div class="widget-body">
					 						 <div>
					 								 <div class="clearfix">
					 										 <div class="btn-group">
					 												 <button id="editable-sample_new" class="btn green" id="dynamic-table" class="table table-striped table-bordered table-hover">
					 														 Adicionar <i class="icon-plus"></i>
					 												 </button>
					 										 </div>
															 <div class="clearfix">
																 <div class="pull-right tableTools-container"></div>
															 </div>
					 										 <div class="btn-group pull-right">
					 												 <button class="btn dropdown-toggle" data-toggle="dropdown" onclick="printPage();">Print <i class="icon-angle-down" class ></i>
					 												    <script>
					 												 function printPage() {
				
																			if (window.print) {

																			agree = confirm("Deseja imprimir essa pagina ?");

																				if (agree) {
																				window.print();

																				if (_GET("pg") != null)
																				location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
																				else
																				history.go(-1);


																					window.close();
																									}



																														}
																											}
																			</script>
					 												 </button>
					 												 <ul class="dropdown-menu pull-right">
					 														 <li> teste</a></li>
					 														 <li><a href="#">Save as PDF</a></li>
					 														 <li><a href="#">Export to Excel</a></li>
					 												 </ul>
					 										 </div>
					 								 </div>
					 								 <div class="space15"></div>
					 								 <table class="table table-striped table-hover table-bordered" id="editable-sample">
					 										 <thead>
					 										 <tr>
					 												 <th>Material</th>
					 												 <th>Cod</th>
					 												 <th>Qtd</th>
					 												 <th>Obs</th>
					 												 <th>Editar</th>
					 												 <th>Deletar</th>
					 										 </tr>
					 										 </thead>
					 										 <tbody>

					 										 </tbody>
					 								 </table>
					 						 </div>
					 				 </div>
					 		 </div>



								<div id="my-modal" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h3 class="smaller lighter blue no-margin">A modal with a slider in it!</h3>
											</div>

											<div class="modal-body">
												Some content
												<br />
												<br />
												<br />
												<br />
												<br />
												1
												<br />
												<br />
												<br />
												<br />
												<br />
												2
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Fechar
												</button>
											</div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div>

								<div id="aside-inside-modal" class="modal" data-placement="bottom" data-background="true" data-backdrop="false" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12 white">
														<h3 class="lighter no-margin">Inside another modal</h3>

														<br />
														<br />
													</div>
												</div>
											</div>
										</div><!-- /.modal-content -->

										<button class="btn btn-default btn-app btn-xs ace-settings-btn aside-trigger" data-target="#aside-inside-modal" data-toggle="modal" type="button">
											<i data-icon2="fa-arrow-down" data-icon1="fa-arrow-up" class="ace-icon fa fa-arrow-up bigger-110 icon-only"></i>
										</button>
									</div><!-- /.modal-dialog -->
								</div>

								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->