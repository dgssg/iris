	
					<div class="alert alert-block alert-success">
						<button type="button" class="close" data-dismiss="alert">
							<i class="ace-icon fa fa-times"></i>
						</button>

						<i class="ace-icon fa fa-check green"></i>

						Bem vindo ao
						<strong class="green">
							IRIS
							<small>(v1.4)</small>
						</strong>,
		sistema integrado e com conectividade <a href="https://www.cvhealthcare.me">MK Sistemas Biomédicos</a> (General CME Prospect).
					</div>


<div class="right_col" role="main">
	<div class="">
		<div class="page-title">


		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Supervisão</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						
					</div>
					
                       
                        <span class="btn btn-app btn-sm btn-primary no-hover" style="width:150px;">
													<span class="line-height-1 bigger-170 "> Total 
		<?php	include("backend/dashboard-carregamento-total-backend.php"); ?>
													</span>

													<br />
													<span class="line-height-1 smaller-100"> Carregamentos </span>
												</span>
                     <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170">Total 	<?php	include("backend/dashboard-descarregamento-total-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Descarregameto </span>
												</span>
                        <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170">Total  <?php	include("backend/dashboard-montagemkit-total-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Montagem Kit </span>
												</span>
                   <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170">Total  <?php	include("backend/dashboard-eventos-total-backend.php"); ?>  </span>

													<br />
													<span class="line-height-1 smaller-100">Eventos </span>
												</span>
                   <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Total <?php	include("backend/dashboard-anomalia-total-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Anomalia </span>
												</span>
                 <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Total <?php	include("backend/dashboard-assistencial-total-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Assistencial </span>
												</span>
                    <span class="btn btn-app btn-sm btn-success no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Atual 	<?php	include("backend/dashboard-carregamento-atual-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Carregamento </span>
												</span>
                         
               
               							

			</div>

			<div class="col-md-6 col-sm-6  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Eventos</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

					<object  style="width:100%; height:300px"   type="text/html"  data="php/dashboard/eventos/analitico.php" > </object>

					</div>
				</div>
			</div>


			<div class="col-md-4 col-sm-4  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Anomalia</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<object  style="width:100%; height:300px"   type="text/html"  data="php/dashboard/anomalia/analitico.php" > </object>

					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Assistencial</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

				<object  style="width:100%; height:400px"   type="text/html"  data="php/dashboard/assistencial/analitico.php" > </object>

					</div>
				</div>
			</div>

			


		<!--	<div class="col-md-6 col-sm-6  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Scatter Graph</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<div id="echart_scatter" style="height:350px;"></div>

					</div>
				</div>
			</div> -->

	<!--		<div class="col-md-6 col-sm-6  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Montagem</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<object  style="width:100%; height:400px"   type="text/html"  data="php/dashboard/montagemkit/analitico.php" > </object>

					</div>
				</div>
			</div>

	

		<!--	<div class="col-md-8 col-sm-8  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>World Map</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<div id="echart_world_map" style="height:370px;"></div>

					</div>
				</div>
			</div> -->


	<!--		<div class="col-md-4 col-sm-4  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Acondicionamento</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

					<object  style="width:100%; height:400px"   type="text/html"  data="php/dashboard/acondicionamento/analitico.php" > </object>

					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Ciclo</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<object  style="width:100%; height:400px"   type="text/html"  data="php/dashboard/carregamento/analitico.php" > </object>

					</div>
				</div>
			</div>


		<!--	<div class="col-md-4 col-sm-4  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Gauge</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div id="echart_gauge" style="height:370px;"></div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>
