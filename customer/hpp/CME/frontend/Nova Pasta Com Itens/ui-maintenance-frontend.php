  	<?php
include("database/database.php");
?>
					<div class="alert alert-block alert-success">
						<button type="button" class="close" data-dismiss="alert">
							<i class="ace-icon fa fa-times"></i>
						</button>

						<i class="ace-icon fa fa-check green"></i>

						Bem vindo ao
						<strong class="green">
							IRIS
							<small>(v1.4)</small>
						</strong>,
		sistema integrado e com conectividade <a href="https://www.cvhealthcare.me">MK Sistemas Biomédicos</a> (General CME Prospect).
					</div>


<div class="right_col" role="main">
	<div class="">
		<div class="page-title">


		</div>

		<div class="clearfix"></div>
	<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-5">
									<div class="widget-box">
										<div class="widget-header">
											<h4 class="widget-title">Cadastro Manutenções</h4>

											<span class="widget-toolbar">
												<a href="#" data-action="settings">
													<i class="ace-icon fa fa-cog"></i>
												</a>

												<a href="#" data-action="reload">
													<i class="ace-icon fa fa-refresh"></i>
												</a>

												<a href="#" data-action="collapse">
													<i class="ace-icon fa fa-chevron-up"></i>
												</a>

												<a href="#" data-action="close">
													<i class="ace-icon fa fa-times"></i>
												</a>
											</span>
										</div>
	<div class="hr hr-dotted"></div>
										<form class="form-horizontal" action="php/jquery-ui.php" method="post" >

                          	<?php



$query = "SELECT * FROM cvheal47_iris_hpp.equipamento";


if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
     $stmt->bind_result($id,$equipamento,$codigo,$metodo,$capacidade,$potencia,$validacao,$modelo,$reg_date,$update);
   
?>


	<div class="col-md-7 col-sm-7  form-group has-feedback">
										<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"></label>

										<div class="col-xs-12 col-sm-12">
											<select  name="equipamento" id="equipamento">
												<?php	 while ($stmt->fetch()) { ?>
										<option value="<?php printf($codigo);?>	"><?php printf($codigo);?> <p><?php printf($equipamento);?> <p><?php printf($modelo);?>	</option>

											<?php
											// tira o resultado da busca da memória
											} 

											?>


											</select>
										</div>
									</div>
									 <script>
        $(document).ready(function() {
            $('#equipamento').select2();
        });
    </script>

 <?php  } $stmt->close();?>

										<!--	<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="codigo"></label>

																												<div class="col-xs-6 col-sm-6">
																														<div class="input-group">
																															<span class="input-group-addon">
																																<i class="ace-icon fa fa-qrcode"></i>
																															</span>
														<input type="text" name="codigo" id="codigo" class="col-xs-6 col-sm-6" placeholder="Codigo"/>
													</div>
												</div>
											</div>  -->
												


																
													<div class="col-md-12 col-sm-12  form-group has-feedback">
																						<div class="input-group">
																							<span class="input-group-addon">
																								<i class="ace-icon glyphicon glyphicon-asterisk"></i>
																							</span>
																					<select name="manutencao" id="manutencao" class="control-label col-xs-12 col-sm-5 no-padding-right"  class="chosen-select form-control"type="text" >
																					<option value=""> Selecione </option>
																					<option value="C">Corretiva</option>
																					<option value="P">Preventiva</option>


																					</select>
																			</div>
																			</div>
																				
																				

																					
																					<label class="control-label col-xs-12 col-sm-12 no-padding-right"  for="data"> </label>
         																	<!--<label for="id-date-picker-1"></label> -->


																							<div class="col-xs-6 col-sm-6">
																								<div class="input-group">
																									<input class="form-control date-picker" id="data" type="text" data-date-format="dd-mm-yyyy" name="data" placeholder="Data"/>
																									<span class="input-group-addon">
																										<i class="fa fa-calendar bigger-110"></i>
																									</span>
																								</div>
																							</div>
																						
																						




																


																
																<div class="ln_solid"></div>
									<div class="item form-group ">
										
										<div class="col-md-10 col-sm-10 offset-md-3">
																	<button type="button" class="btn btn-primary" id="bootbox-confirm"
																	onclick="new PNotify({
																						title: 'Cancelado',
																						text: 'Registro Cancelado',
																						styling: 'bootstrap3'
																				});" >Cancelar</button>
																	<button type="reset" class="btn btn-primary"id="bootbox-confirm" onclick="new PNotify({
																						title: 'Limpado',
																						text: 'Todos os Campos Limpos',
																						type: 'info',
																						styling: 'bootstrap3'
																				});"  >Limpar</button>
																	<input type="submit" class="btn btn-primary"id="bootbox-confirm" onclick="new PNotify({
																						title: 'Registrado',
																						text: 'Informações registrada!',
																						type: 'success',
																						styling: 'bootstrap3'
																				});" />

																					</div>
												</div>
											</div>
										</form>

									
					



	</div>
	</div>
</div>

	
		</div>
	</div>
</div>
		