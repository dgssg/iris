	<!-- PAGE CONTENT BEGINS -->
					<div class="alert alert-block alert-success">
						<button type="button" class="close" data-dismiss="alert">
							<i class="ace-icon fa fa-times"></i>
						</button>

						<i class="ace-icon fa fa-check green"></i>

						Bem vindo ao
						<strong class="green">
							IRIS
							<small>(v1.4)</small>
						</strong>,
		sistema integrado e com conectividade <a href="https://www.cvhealthcare.me">CV Healthcare</a> (General CME Prospect).
					</div>


<div class="right_col" role="main">
	<div class="">
		<div class="page-title">


		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Supervisão</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						
					</div>
					
                       
                        <span class="btn btn-app btn-sm btn-primary no-hover" style="width:150px;">
													<span class="line-height-1 bigger-170 "> Total 	<?php	include("backend/dashboard-2-instrumental-total-backend.php"); ?>  </span>

													<br />
													<span class="line-height-1 smaller-100"> Instrumentais </span>
												</span>
                     <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170">Total  <?php	include("backend/dashboard-2-material-total-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Materiais </span>
												</span>
                        <span class="btn btn-app btn-sm btn-primary no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170">Total  <?php	include("backend/dashboard-2-kit-total-backend.php"); ?> </span>

													<br />
													<span class="line-height-1 smaller-100"> Kits </span>
												</span>
                 
												
                    <span class="btn btn-app btn-sm btn-success no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Arsenal <object data="php/dashboard2/instrumentalarsenal/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Instrumental </span>
												</span>
												 <span class="btn btn-app btn-sm btn-success no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Arsenal <object data="php/dashboard2/materialarsenal/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Material </span>
												</span>
												 <span class="btn btn-app btn-sm btn-success no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Arsenal <object data="php/dashboard2/kitarsenal/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Kit </span>
												</span>
												
												   <span class="btn btn-app btn-sm btn-danger no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Expurgo <object data="php/dashboard2/instrumentalarsenal/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Instrumental </span>
												</span>
												 <span class="btn btn-app btn-sm btn-danger no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Expurgo <object data="php/dashboard2/materialarsenal/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Material </span>
												</span>
												 <span class="btn btn-app btn-sm btn-danger no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Expurgo <object data="php/dashboard2/kitarsenal/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Kit </span>
												</span>
												<span class="btn btn-app btn-sm btn-inverse no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Externo <object data="php/dashboard2/manutencaoinstrumental/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Instrumental </span>
												</span>
													<span class="btn btn-app btn-sm btn-warning no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Novo <object data="php/dashboard2/novoinstrumental/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Instrumental </span>
												</span>
													<span class="btn btn-app btn-sm btn-pink no-hover"  style="width:150px;">
													<span class="line-height-1 bigger-170"> Desativado <object data="php/dashboard2/desativadoinstrumental/geral.php" > </object> </span>

													<br />
													<span class="line-height-1 smaller-100"> Instrumental </span>
												</span>
                         
               
               							

			</div>
<div class="row">
										<div class="col-xs-12 col-sm-10 widget-container-col" id="widget-container-col-3">
											<div class="widget-box widget-color-orange collapsed" id="widget-box-3">
												<div class="widget-header widget-header-small">
													<h6 class="widget-title">
														<i class="ace-icon fa fa-sort"></i>
														Informação Arsenal
													</h6>

													<div class="widget-toolbar">
														<a href="#" data-action="settings">
															<i class="ace-icon fa fa-cog"></i>
														</a>

														<a href="#" data-action="reload">
															<i class="ace-icon fa fa-refresh"></i>
														</a>

														<a href="#" data-action="collapse">
															<i class="ace-icon fa fa-plus" data-icon-show="fa-plus" data-icon-hide="fa-minus"></i>
														</a>

														<a href="#" data-action="close">
															<i class="ace-icon fa fa-times"></i>
														</a>
													</div>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															Relatorio Dashboard Atualizado.
														</p>
													</div>
												</div>
											</div>
										</div>
										
										
										<div class="hr hr32 hr-dotted"></div>

								<div class="row">
									<div class="col-sm-5">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title lighter">
													<i class="ace-icon fa fa-star orange"></i>
													Materiais Externos
												</h4>

												<div class="widget-toolbar">
													<a href="#" data-action="collapse">
														<i class="ace-icon fa fa-chevron-up"></i>
													</a>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>
																	<i class="ace-icon fa fa-caret-right blue"></i>Material
																</th>

																<th>
																	<i class="ace-icon fa fa-caret-right blue"></i>Codigo
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right blue"></i>status
																</th>
															</tr>
														</thead>

														<tbody>
															<tr>
																<td>Material enviado</td>

																<td>
																
																	<b class="green">123</b>
																</td>

																<td class="hidden-480">
																	<span class="label label-info arrowed-right arrowed-in">status material</span>
																</td>
															</tr>

															

														
														
														
														
														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->
										
										
										
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

					<object  style="width:100%; height:400px"   type="text/html"  data="php/dashboard2/kitarsenal/analitico.php" > </object>

					</div>
				</div>
		
				<div class="x_panel">
					<div class="x_title">
						<h2></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<object  style="width:100%; height:400px"   type="text/html"  data="php/dashboard2/materialarsenal/analitico.php" > </object>

					</div>
				</div>
			</div>

		

			


		<!--	<div class="col-md-6 col-sm-6  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Scatter Graph</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<div id="echart_scatter" style="height:350px;"></div>

					</div>
				</div>
			</div> -->

		
	

		<!--	<div class="col-md-8 col-sm-8  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>World Map</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<div id="echart_world_map" style="height:370px;"></div>

					</div>
				</div>
			</div> -->


		

			


		<!--	<div class="col-md-4 col-sm-4  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Gauge</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">Parametro 1</a>
										<a class="dropdown-item" href="#">Parametro 2</a>
									</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div id="echart_gauge" style="height:370px;"></div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>
<!-- /page content -->