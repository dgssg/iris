
<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Unidade</h3>
									
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header">
											Consulta  
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										<div>
											<table id="dynamic-table" class="table table-striped table-bordered table-hover">
											<thead>
                        <tr>
                        <th>#</th>
                        <th>Codigo</th>
                                                           
						<th>Unidade</th>
					
                                                             
                                                        
                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                      
                       
                                                            
                     
                      </tbody>

											
											</table>
										</div>
									</div>
								</div>

							
								<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
						

								<!-- PAGE CONTENT ENDS -->
								<script type="text/javascript">
 
 $(document).ready(function() {
// Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-unidade.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
.then(response => response.json())
.then(data => {
// Mapeia os dados para o formato esperado pelo DataTables
const novosDados = data.map(item => [
`	<label class="pos-rel">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>`,


item.codigo,
item.unidade,


` <div class="hidden-sm hidden-xs action-buttons">
																

																<a class="green" href="register-unidade-edit.php?id=${item.id}">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>

																<a class="red"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-unidade-trash.php?id=${item.id}';
  }
})
">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</div>

															<div class="hidden-md hidden-lg">
																<div class="inline pos-rel">
																	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																	</button>

																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="ace-icon fa fa-search-plus bigger-120"></i>
																				</span>
																			</a>
																		</li>

																		<li>
																			<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																				<span class="green">
																					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																				</span>
																			</a>
																		</li>

																		<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="ace-icon fa fa-trash-o bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>`
]);

// Adiciona as novas linhas ao DataTables e desenha a tabela
$('#dynamic-table').DataTable().rows.add(novosDados).draw();


});
});
</script>