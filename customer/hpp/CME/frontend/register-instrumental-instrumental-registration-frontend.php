	<!-- PAGE CONTENT BEGINS -->

	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

	<form id="regForm" action="backend/action_page_edit.php">
	    <div class="widget-box">
	        <div class="widget-header widget-header-blue widget-header-flat">
	            <h4 class="widget-title lighter">Novo Cadastro</h4>

	            <div class="widget-toolbar">
	                <label>
	                    <small class="green">
	                        <b>Scanner</b>
	                    </small>

	                    <input id="skip-validation" type="checkbox" class="ace ace-switch ace-switch-4" />
	                    <span class="lbl middle"></span>
	                </label>
	            </div>
	        </div>

	        <div class="widget-body">
	            <div class="widget-main">
	                <div id="fuelux-wizard-container">
	                    <div>
	                        <ul class="steps">
	                            <li data-step="1" class="active">
	                                <span class="step">1</span>
	                                <span class="title">Etapa 1</span>
	                            </li>

	                            <li data-step="2">
	                                <span class="step">2</span>
	                                <span class="title">Etapa 2</span>
	                            </li>

	                            <li data-step="3">
	                                <span class="step">3</span>
	                                <span class="title">Etapa 3</span>
	                            </li>

	                            <li data-step="4">
	                                <span class="step">4</span>
	                                <span class="title">Etapa 4</span>
	                            </li>
	                        </ul>
	                    </div>

	                    <hr />

	                    <div class="step-content pos-rel">
	                        <div class="step-pane active" data-step="1">


	                            <div class="form-group ">
	                                <label for="nome"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">Familia</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <select class="chosen-select form-control" name="id_instrumental_subgrupo"
	                                            id="form-field-select-3" data-placeholder="Selecione um grupo">
	                                            <option>Selecione um Grupo</option>
	                                        </select>
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="nome"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">Catalogo</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <select class="chosen-select form-control" name="id_catalogo"
	                                            id="form-field-select" data-placeholder="Selecione um grupo">
	                                            <option>Selecione um Grupo</option>
	                                        </select>
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="nome"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">Nome</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="text" id="nome" name="nome" class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="patrimonio"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">Patrimonio</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="text" id="patrimonio" name="patrimonio"
	                                            class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="serie"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">N/S</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="text" id="serie" name="serie" class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>


	                            <div class="form-group ">
	                                <label for="cod"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">Codigo</label>


	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="text" id="cod" name="cod" class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="data_inst" class="col-xs-12 col-sm-3 control-label no-padding-right">Data
	                                    Instalação</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="date" id="date_inst" name="date_inst"
	                                            class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="vlr"
	                                    class="col-xs-12 col-sm-3 control-label no-padding-right">Valor</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="text" id="vlr" name="vlr" class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>

	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>

	                            <div class="form-group ">
	                                <label for="cod" class="col-xs-12 col-sm-3 control-label no-padding-right">NF</label>


	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <input type="text" id="nf" name="nf" class="col-xs-12 col-sm-5" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="space-2"></div>

	                            <div class="hr hr-dotted"></div>













	                            <div class="space-2"></div>



	                            <div class="space-2"></div>

	                            <div class="form-group">
	                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
	                                    for="comment">Observação</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div class="clearfix">
	                                        <textarea class="input-xlarge" name="obs" id="obs"></textarea>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="space-8"></div>


	                            <!-- Botão "Próximo" -->
	                            <button class="btn btn-next btn-primary" type="button">Próximo</button>



	                        </div>

	                        <div id="fuelux-wizard-container" class="step-pane" data-step="2">
	                            <div>
	                                <div class="form-group ">
	                                    <label for="nome"
	                                        class="col-xs-12 col-sm-3 control-label no-padding-right">Unidade</label>

	                                    <div class="col-xs-12 col-sm-9">
	                                        <div class="clearfix">
	                                            <select class="chosen-select form-control" name="unidade"
	                                                id="form-field-select-3" data-placeholder="Selecione uma Unidade">
	                                                <option>Selecione uma Unidade</option>
	                                            </select>
	                                        </div>
	                                    </div>

	                                </div>
	                                <div class="space-2"></div>

	                                <div class="hr hr-dotted"></div>
	                                <div class="form-group ">
	                                    <label for="nome"
	                                        class="col-xs-12 col-sm-3 control-label no-padding-right">Setor</label>

	                                    <div class="col-xs-12 col-sm-9">
	                                        <div class="clearfix">
	                                            <select class="chosen-select form-control" name="setor"
	                                                id="form-field-select-setor" data-placeholder="Selecione um Setor">
	                                                <option>Selecione um Setor</option>
	                                            </select>
	                                        </div>
	                                    </div>

	                                </div>
	                                <div class="space-2"></div>

	                                <div class="hr hr-dotted"></div>
	                                <div class="form-group ">
	                                    <label for="nome"
	                                        class="col-xs-12 col-sm-3 control-label no-padding-right">Area</label>

	                                    <div class="col-xs-12 col-sm-9">
	                                        <div class="clearfix">
	                                            <select class="chosen-select form-control" name="area"
	                                                id="form-field-select-area" data-placeholder="Selecione uma Area">
	                                                <option>Selecione uma Area</option>
	                                            </select>
	                                        </div>
	                                    </div>

	                                </div>
	                                <div class="space-2"></div>

	                                <div class="hr hr-dotted"></div>






	                            </div>
	                            <!-- Botão "Próximo" -->
	                            <button class="btn btn-next btn-primary" type="button">Próximo</button>

	                            <!-- Botão "Anterior" -->
	                            <button class="btn btn-prev btn-default" type="button">Anterior</button>

	                        </div>

	                        <div id="fuelux-wizard-container" class="step-pane" data-step="3">
	                            <div class="center">
	                                <h3 class="blue lighter">This is step 3</h3>

	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-xs-12 col-sm-3 no-padding-right">Subscribe to</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div>
	                                        <label>
	                                            <input name="subscription" value="1" type="checkbox" class="ace" />
	                                            <span class="lbl"> Latest news and announcements</span>
	                                        </label>
	                                    </div>

	                                    <div>
	                                        <label>
	                                            <input name="subscription" value="2" type="checkbox" class="ace" />
	                                            <span class="lbl"> Product offers and discounts</span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="space-2"></div>

	                            <div class="form-group">
	                                <label class="control-label col-xs-12 col-sm-3 no-padding-right">Gender</label>

	                                <div class="col-xs-12 col-sm-9">
	                                    <div>
	                                        <label class="line-height-1 blue">
	                                            <input name="gender" value="1" type="radio" class="ace" />
	                                            <span class="lbl"> Male</span>
	                                        </label>
	                                    </div>

	                                    <div>
	                                        <label class="line-height-1 blue">
	                                            <input name="gender" value="2" type="radio" class="ace" />
	                                            <span class="lbl"> Female</span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="hr hr-dotted"></div>
	                            <!-- Botão "Próximo" -->
	                            <button class="btn btn-next btn-primary" type="button">Próximo</button>

	                            <!-- Botão "Anterior" -->
	                            <button class="btn btn-prev btn-default" type="button">Anterior</button>

	                        </div>

	                        <div id="fuelux-wizard-container" class="step-pane" data-step="4">
	                            <div class="center">
	                                <h3 class="green">Congrats!</h3>
	                                Your product is ready to ship! Click finish to continue!
	                            </div>
	                            <!-- Botão "Próximo" -->
	                            <button class="btn btn-next btn-primary" type="submit">Salvar</button>

	                            <!-- Botão "Anterior" -->
	                            <button class="btn btn-prev btn-default" type="button">Anterior</button>

	                        </div>
	                    </div>
	                </div>

	                <hr />
	                <div class="wizard-actions">

	                </div><!-- /.widget-main -->
	            </div><!-- /.widget-body -->
	        </div>


	    </div><!-- /.col -->

	    <script>
	    $(document).ready(function() {
	        // Esconder todas as etapas, exceto a primeira
	        $(".step-pane").not(":first").hide();

	        // Variável para armazenar o número da etapa atual
	        var currentStepNumber = 1;

	        // Manipular o clique do botão "Próximo"
	        $(".btn-next").click(function() {
	            var currentStep = $(this).closest(".step-pane");
	            var nextStep = currentStep.next(".step-pane");

	            // Validar os campos da etapa atual antes de avançar
	            if (!validateStep(currentStep)) {
	                return false;
	            }

	            // Atualizar o número da etapa atual
	            currentStepNumber++;

	            // Adicionar classe "active" ao próximo elemento <li>
	            $(".steps li").removeClass("active");
	            $(".steps li[data-step='" + currentStepNumber + "']").addClass("active");

	            // Esconder a etapa atual e mostrar a próxima etapa
	            currentStep.hide();
	            nextStep.show();
	        });

	        // Manipular o clique do botão "Anterior"
	        $(".btn-prev").click(function() {
	            var currentStep = $(this).closest(".step-pane");
	            var prevStep = currentStep.prev(".step-pane");

	            // Validar os campos da etapa atual antes de voltar
	            if (!validateStep(currentStep)) {
	                return false;
	            }

	            // Atualizar o número da etapa atual
	            currentStepNumber--;

	            // Adicionar classe "active" ao elemento <li> anterior
	            $(".steps li").removeClass("active");
	            $(".steps li[data-step='" + currentStepNumber + "']").addClass("active");

	            // Esconder a etapa atual e mostrar a etapa anterior
	            currentStep.hide();
	            prevStep.show();
	        });

	        // Função para validar os campos da etapa atual
	        function validateStep(step) {
	            // Implemente a validação dos campos da etapa atual aqui
	            // Retorne true se a validação for bem-sucedida, ou false caso contrário
	            return true;
	        }
	    });
	    </script>
	    <script>
	    $(document).ready(function() {
	        // faz a chamada AJAX para buscar as opções do grupo
	        $.ajax({
	            url: 'search/search-instrumental-subgroup.php',
	            method: 'GET',
	            dataType: 'json'
	        }).done(function(response) {
	            // se a chamada for bem-sucedida, adiciona as opções ao select
	            var select = $('#form-field-select-3');
	            $.each(response, function(index, group) {
	                select.append('<option value="' + group.id + '">' + group.nome + ' - ' + group
	                    .codigo + ' - ' + group.fabricante + ' - ' + group.modelo + '</option>'
	                    );
	            });
	            // ativa o plugin chosen para tornar o select com filtro
	            select.chosen();
	        }).fail(function(jqXHR, textStatus, errorThrown) {
	            // se a chamada falhar, mostra uma mensagem de erro
	            console.log('Erro ao buscar opções do grupo: ' + textStatus + ' - ' + errorThrown);
	        });
	    });

	    if (!ace.vars['touch']) {
	        $('.chosen-select').chosen({
	            allow_single_deselect: true
	        });
	        //resize the chosen on window resize

	        $(window)
	            .off('resize.chosen')
	            .on('resize.chosen', function() {
	                $('.chosen-select').each(function() {
	                    var $this = $(this);
	                    $this.next().css({
	                        'width': $this.parent().width()
	                    });
	                })
	            }).trigger('resize.chosen');
	        //resize chosen on sidebar collapse/expand
	        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
	            if (event_name != 'sidebar_collapsed') return;
	            $('.chosen-select').each(function() {
	                var $this = $(this);
	                $this.next().css({
	                    'width': $this.parent().width()
	                });
	            })
	        });
	    };
	    </script>
	    <script>
	    $(document).ready(function() {
	        // faz a chamada AJAX para buscar as opções do grupo
	        $.ajax({
	            url: 'search/search-catalogo.php',
	            method: 'GET',
	            dataType: 'json'
	        }).done(function(response) {
	            // se a chamada for bem-sucedida, adiciona as opções ao select
	            var select = $('#form-field-select');
	            $.each(response, function(index, group) {
	                select.append('<option value="' + group.id + '">' + group.nome + ' - ' + group
	                    .codigo + ' - ' + group.material + ' - ' + group.familia + '</option>');
	            });
	            // ativa o plugin chosen para tornar o select com filtro
	            select.chosen();
	        }).fail(function(jqXHR, textStatus, errorThrown) {
	            // se a chamada falhar, mostra uma mensagem de erro
	            console.log('Erro ao buscar opções do grupo: ' + textStatus + ' - ' + errorThrown);
	        });
	    });

	    if (!ace.vars['touch']) {
	        $('.chosen-select').chosen({
	            allow_single_deselect: true
	        });
	        //resize the chosen on window resize

	        $(window)
	            .off('resize.chosen')
	            .on('resize.chosen', function() {
	                $('.chosen-select').each(function() {
	                    var $this = $(this);
	                    $this.next().css({
	                        'width': $this.parent().width()
	                    });
	                })
	            }).trigger('resize.chosen');
	        //resize chosen on sidebar collapse/expand
	        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
	            if (event_name != 'sidebar_collapsed') return;
	            $('.chosen-select').each(function() {
	                var $this = $(this);
	                $this.next().css({
	                    'width': $this.parent().width()
	                });
	            })
	        });
	    };
	    </script>
        <script>
    $(document).ready(function() {
  // faz a chamada AJAX para buscar as opções do grupo
  $.ajax({
    url: 'search/search-unidade.php',
    method: 'GET',
    dataType: 'json'
  }).done(function(response) {
    // se a chamada for bem-sucedida, adiciona as opções ao select
    var select = $('#form-field-select-3');
    $.each(response, function(index, group) {
      select.append('<option value="' + group.id + '">' + group.unidade + ' - ' + group.codigo + '</option>');
    });
    // ativa o plugin chosen para tornar o select com filtro
    select.chosen();
  }).fail(function(jqXHR, textStatus, errorThrown) {
    // se a chamada falhar, mostra uma mensagem de erro
    console.log('Erro ao buscar opções do grupo: ' + textStatus + ' - ' + errorThrown);
  });
});

if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
                };
</script>

<script type="text/javascript">
    
    $(document).ready(function(){
    $('#form-field-select-3').change(function(){
        $('#form-field-select-setor').load('search/search-setor.php?id='+$('#form-field-select-3').val(), function() {
            $('#form-field-select-setor').chosen();
        });
    });
});


                </script>
                <script type="text/javascript">
    
    $(document).ready(function(){
    $('#form-field-select-setor').change(function(){
        $('#form-field-select-area').load('search/search-area.php?id='+$('#form-field-select-setor').val(), function() {
            $('#form-field-select-area').chosen();
        });
    });
});


                </script>