
<div class="col-xs-6 col-sm-4">
    <div class="widget-box">
      <div class="widget-header">
        <h4 class="widget-title">Cadastro Grupo</h4>
        <span class="widget-toolbar">
          <a href="#" data-action="settings">
            <i class="ace-icon fa fa-cog"></i>
          </a>
          <a href="#" data-action="reload">
            <i class="ace-icon fa fa-refresh"></i>
          </a>
          <a href="#" data-action="collapse">
            <i class="ace-icon fa fa-chevron-up"></i>
          </a>
          <a href="#" data-action="close">
            <i class="ace-icon fa fa-times"></i>
          </a>
        </span>
      </div>
      <form class="form-horizontal" action="backend/register-supplier-group-registration-backend.php" onsubmit="return showConfirmationDialog(event)" method="POST">
      <div class="widget-body">
        <div class="widget-main">
          <div>
            <label for="nome">
              Nome
            </label>
            <div class="input-group">
              <input class="form-control" type="text" name="nome" id="nome" />
            </div>
          </div>
          <hr />
       
          <div>
          <button class="btn btn-primary" type="submit">Salvar</button>
          

          </div>
        </div>
      </div>
      </form>
    </div>
  </div>


  <script>
function showConfirmationDialog(event) {
  event.preventDefault(); // previne o envio do formulário
  Swal.fire({
    title: 'Deseja salvar as alterações?',
    showDenyButton: true,
    showCancelButton: true,
    confirmButtonText: 'Salvar',
    denyButtonText: 'Não salve',
  }).then((result) => {
    if (result.isConfirmed) {
      event.target.submit(); // Envia o formulário
    } else if (result.isDenied) {
      Swal.fire('As alterações não são salvas', '', 'info');
    }
  });
}
</script>

</div>
  </div>