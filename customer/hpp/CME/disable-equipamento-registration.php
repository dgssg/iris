<?php
    include("database/database.php");
    session_start();
    if(!isset($_SESSION['usuario'])){
        header ("Location: ../index.php");
    }
    if($_SESSION['id_nivel'] != 1 ){
        header ("Location: ../index.php");
    }
    if($_SESSION['instituicao'] != $key ){
        header ("Location: ../index.php");
    } if( $_SESSION['mod'] != $mod ){
        header ("Location: ../index.php");
    }
    $usuariologado=$_SESSION['usuario'];
    $instituicaologado=$_SESSION['instituicao'];
    $setorlogado=$_SESSION['setor'];
    
    // Adiciona a Função display_campo($nome_campo, $tipo_campo)
    //require_once "personalizacao_display.php";
    
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />
    <title>IRIS - CME</title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="../../../framework/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../../framework/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

    <!-- page specific plugin styles -->
	<link rel="stylesheet" href="../../../framework/assets/css/jquery-ui.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/bootstrap-datepicker3.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/ui.jqgrid.min.css" />
        <link rel="stylesheet" href="../../../framework/assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/jquery.gritter.min.css" />
        <script src="../../../framework/assets/js/tree.min.js"></script>
        <link rel="stylesheet" href="../../../framework/assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/chosen.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/bootstrap-datepicker3.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/bootstrap-timepicker.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/daterangepicker.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/bootstrap-colorpicker.min.css" />
        <link rel="stylesheet" href="../../../framework/assets/css/bootstrap-duallistbox.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/bootstrap-multiselect.min.css" />
		<link rel="stylesheet" href="../../../framework/assets/css/select2.min.css" />

        


    <!-- text fonts -->
    <link rel="stylesheet" href="../../../framework/assets/css/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="../../../framework/assets/css/ace.min.css" class="ace-main-stylesheet"
        id="main-ace-style" />

    <!--[if lte IE 9]>
<link rel="stylesheet"  href="../../../framework/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
    <link rel="stylesheet" href="../../../framework/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../../../framework/assets/css/ace-rtl.min.css" />

    <!--[if lte IE 9]>
<link rel="stylesheet"  href="../../../framework/assets/css/ace-ie.min.css" />
<![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="../../../framework/assets/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
<script src="../../../framework/assets/js/html5shiv.min.js"></script>
<script src="../../../framework/assets/js/respond.min.js"></script>
<![endif]-->
    <style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    </style>

</head>

<body class="no-skin">
    <div id="navbar" class="navbar navbar-default          ace-save-state">
        <div class="navbar-container ace-save-state" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>

            <div class="navbar-header pull-left">
                <a href="dashboard" class="navbar-brand">
                    <small>
                        <i class="fa fa-eye"></i>
                        IRIS |- CME
                    </small>
                </a>
            </div>

            </ul>
        </div>
    </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {}
        </script>

        <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
            <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {}
            </script>

            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                    <button class="btn btn-success">
                        <a href="read-inventory">
                            <i class="ace-icon fa fa-barcode"></i>
                    </button>
                    </a>
                    <button class="btn btn-info">
                        <a href="read-validation">
                            <i class="ace-icon fa fa-check-square-o"></i>
                    </button>
                    </a>

                    <button class="btn btn-warning">
                        <a href="read-catalog">
                            <i class="ace-icon fa fa-list-alt"></i>
                    </button>
                    </a>


                    <button class="btn btn-danger">
                        <a href="read-tag">
                            <i class="ace-icon glyphicon glyphicon-tag"></i>
                    </button>
                    </a>
                </div>

                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span>

                    <span class="btn btn-info"></span>

                    <span class="btn btn-warning"></span>

                    <span class="btn btn-danger"></span>
                </div>

            </div><!-- /.sidebar-shortcuts -->
            <button class="btn btn-white btn-info btn-bold">
                <a href="profile">
                    <i class="ace-icon fa fa-user bigger-140 blue"></i>

            </button> </a> <?php printf($usuariologado) ?> </br>

            Sessão: <p class="glyphicon glyphicon-time" id="countdown"></p> </br>
            Data: <script>
            document.write(new Date().toLocaleDateString());
            </script>
            <ul class="nav nav-list">
                <li>
                    <a href="dashboard">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text"> Dashboard </span>
                    </a>

                    <b class="arrow"></b>
                </li>

                <li>
                    <a href="dashboard-2">
                        <i class="menu-icon fa fa-home"></i>
                        <span class="menu-text">Dashboard Arsenal </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li>
                    <a href="dashboard-3">
                        <i class="menu-icon fa fa-code-fork"></i>
                        <span class="menu-text">Dashboard BI </span>
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-desktop"></i>
                        <span class="menu-text">
                            UI &amp; Mapa
                        </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <!--            <li class="">
<a href="#" class="dropdown-toggle">
<i class="menu-icon fa fa-caret-right"></i>

Layouts
<b class="arrow fa fa-angle-down"></b>
</a>

<b class="arrow"></b>

<ul class="submenu">
<li class="">
<a href="top-menu">
<i class="menu-icon fa fa-caret-right"></i>
Top Menu
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="two-menu-1">
<i class="menu-icon fa fa-caret-right"></i>
Two Menus 1
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="two-menu-2">
<i class="menu-icon fa fa-caret-right"></i>
Two Menus 2
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="mobile-menu-1">
<i class="menu-icon fa fa-caret-right"></i>
Default Mobile Menu
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="mobile-menu-2">
<i class="menu-icon fa fa-caret-right"></i>
Mobile Menu 2
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="mobile-menu-3">
<i class="menu-icon fa fa-caret-right"></i>
Mobile Menu 3
</a>

<b class="arrow"></b>
</li>
</ul>
</li>

<li class="">
<a href="typography">
<i class="menu-icon fa fa-caret-right"></i>
Typography
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="elements">
<i class="menu-icon fa fa-caret-right"></i>
Elements
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="buttons">
<i class="menu-icon fa fa-caret-right"></i>
Buttons &amp; Icons
</a>

<b class="arrow"></b>
</li> -->

                        <li class="">
                            <a href="ui-mapa">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Mapa
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="ui-opme">
                                <i class="menu-icon fa fa-caret-right"></i>
                                OPME
                            </a>

                            <b class="arrow"></b>
                        </li>


                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Manutenção
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Equipamento

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>
                                    <b class="arrow"></b>
                                    <ul class="submenu">

                                        <li class="">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="menu-icon fa fa-home orange"></i>
                                                Categoria

                                                <b class="arrow fa fa-angle-down"></b>
                                            </a>

                                            <b class="arrow"></b>

                                            <ul class="submenu">
                                                <li class="">
                                                    <a href="ui-maintenance-equipament-category-registration">
                                                        <i class="menu-icon fa fa-bug purple"></i>
                                                        Cadastro
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>

                                                <li class="">
                                                    <a href="ui-maintenance-equipament-category-search">
                                                        <i class="menu-icon fa fa-file pink"></i>
                                                        Consulta
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="menu-icon fa fa-home orange"></i>
                                                Manutenção

                                                <b class="arrow fa fa-angle-down"></b>
                                            </a>

                                            <b class="arrow"></b>

                                            <ul class="submenu">
                                                <li class="">
                                                    <a href="ui-maintenance-equipament-maintenance-registration">
                                                        <i class="menu-icon fa fa-bug purple"></i>
                                                        Cadastro
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>

                                                <li class="">
                                                    <a href="ui-maintenance-equipament-maintenance-search">
                                                        <i class="menu-icon fa fa-file pink"></i>
                                                        Consulta
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Instrumental

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>
                                    <b class="arrow"></b>
                                    <ul class="submenu">

                                        <li class="">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="menu-icon fa fa-home orange"></i>
                                                Categoria

                                                <b class="arrow fa fa-angle-down"></b>
                                            </a>

                                            <b class="arrow"></b>

                                            <ul class="submenu">
                                                <li class="">
                                                    <a href="ui-maintenance-instrumental-category-registration">
                                                        <i class="menu-icon fa fa-bug purple"></i>
                                                        Cadastro
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>

                                                <li class="">
                                                    <a href="ui-maintenance-instrumental-category-search">
                                                        <i class="menu-icon fa fa-file pink"></i>
                                                        Consulta
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="menu-icon fa fa-home orange"></i>
                                                Manutenção

                                                <b class="arrow fa fa-angle-down"></b>
                                            </a>

                                            <b class="arrow"></b>

                                            <ul class="submenu">
                                                <li class="">
                                                    <a href="ui-maintenance-instrumental-maintenance-registration">
                                                        <i class="menu-icon fa fa-bug purple"></i>
                                                        Cadastro
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>

                                                <li class="">
                                                    <a href="ui-maintenance-instrumental-maintenance-search">
                                                        <i class="menu-icon fa fa-file pink"></i>
                                                        Consulta
                                                    </a>

                                                    <b class="arrow"></b>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="">
                            <a href="ui-prevision">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Previsão &amp; Provisão
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Eventos
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Categoria

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="ui-event-category-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="ui-event-category-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="ui-event-group-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="ui-event-group-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Eventos

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="ui-event-event-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="ui-event-event-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>


                            </ul>

                        </li>

                    </ul>
                </li>

                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-list"></i>
                        <span class="menu-text"> Kits &amp; Material </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="">
                            <a href="inventory-material">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Material
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="inventory-kit">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Inventario Kit
                            </a>

                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-pencil-square-o"></i>
                        <span class="menu-text"> Registro </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Instrumental
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-instrumental-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-instrumental-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-instrumental-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-instrumental-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Instrumental

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-instrumental-instrumental-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-instrumental-instrumental-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Material
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-material-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-material-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-material-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-material-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Material

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-material-material-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-material-material-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Containner
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-containner-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-containner-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-containner-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-containner-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Containner

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-containner-containner-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-containner-containner-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Caixa
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-box-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-box-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-box-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-box-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Caixa

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-box-box-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-box-box-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Equipamento
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-equipament-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-equipament-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-equipament-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-equipament-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Equipamento

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-equipament-equipament-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-equipament-equipament-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Localização
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Unidade

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-location-unidade-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-location-unidade-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Setor

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-location-setor-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-location-setor-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Area

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-location-area-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-location-area-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>





                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Usuario & Colaborador
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Usuario

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-user-user-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-user-user-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Colaborador

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-user-collaborator-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-user-collaborator-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                            </ul>

                        </li>



                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Catalogo
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-catalog-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-catalog-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-catalog-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-catalog-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Catalogo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-catalog-catalog-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-catalog-catalog-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Fornecedor
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-supplier-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-supplier-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-supplier-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-supplier-group-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Fornecedor

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-supplier-supplier-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-supplier-supplier-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Procedimento
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Grupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-procedure-group-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-procedure-group-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        SubGrupo

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-procedure-subgroup-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-procedure-subgroup-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Procedimento

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="register-procedure-procedure-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="register-procedure-procedure-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>



                    </ul>

                </li>
                <li class="">

                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-cogs"></i>
                        <span class="menu-text"> Configurações </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>



                    <ul class="submenu">

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Categoria

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-category-registration">
                                        <i class="menu-icon fa fa-bug purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-category-search">
                                        <i class="menu-icon fa fa-file pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Ciclo

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-cicle-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-cicle-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Especialidade

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-specialty-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-specialty-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Status Pedido

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-status-order-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-status-order-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Status KIT

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-status-kit-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-status-kit-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Status Montagem

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-status-assembly-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-status-assembly-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Status Operação

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-status-operation-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-status-operation-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Status Rack

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-status-rack-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-status-rack-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Workflow

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-workflow-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-workflow-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                API

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-api-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-api-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Parametrização

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-parameterization-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-parameterization-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Status Baixa

                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">
                                    <a href="settings-status-disable-registration">
                                        <i class="menu-icon fa fa-plus purple"></i>
                                        Cadastro
                                    </a>

                                    <b class="arrow"></b>
                                </li>

                                <li class="">
                                    <a href="settings-status-disable-search">
                                        <i class="menu-icon fa fa-eye pink"></i>
                                        Consulta
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>

                    </ul>

                </li>
                <li class="active open">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-trash-o"></i>
                        <span class="menu-text"> Baixa </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Instrumental
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">



                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Instrumental

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="disable-instrumental-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="disable-instrumental-research">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Material
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">



                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Material

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="disable-material-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="disable-material-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Containner
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">




                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Containner

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="disable-containner-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="disable-containner-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Caixa
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">



                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Caixa

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="disable-box-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="disable-box-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                        <li class="active open">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>

                                Equipamento
                                <b class="arrow fa fa-angle-down"></b>
                            </a>

                            <b class="arrow"></b>



                            <ul class="submenu">



                                <li class="active open">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-pencil orange"></i>
                                        Equipamento

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="active">
                                            <a href="disable-equipamento-registration">
                                                <i class="menu-icon fa fa-plus purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="disable-equipamento-search">
                                                <i class="menu-icon fa fa-eye pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="report-report">
                        <i class="menu-icon fa fa-list-alt"></i>
                        <span class="menu-text"> Relatorio </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="report-indicator">
                        <i class="menu-icon fa fa-bar-chart"></i>
                        <span class="menu-text"> Indicadores </span>
                    </a>

                    <b class="arrow"></b>
                </li>


                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-history"></i>
                        <span class="menu-text"> Historico </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="">
                            <a href="historic-assembly">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Montagem
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="historic-preload">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Precarga
                            </a>
                        <li class="">
                            <a href="historic-operation">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Operação
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="historic-opme">
                                <i class="menu-icon fa fa-caret-right"></i>
                                OPME
                            </a>

                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="control-loading">
                        <i class="menu-icon fa fa-camera"></i>
                        <span class="menu-text"> Carregamento </span>
                    </a>

                <li class="">

                <li class="">
                    <a href="control-evolution">
                        <i class="menu-icon fa fa-filter"></i>
                        <span class="menu-text"> Evolução </span>
                    </a>
                <li class="">
                    <a href="control-costs">
                        <i class="menu-icon fa fa-money"></i>
                        <span class="menu-text"> Custos </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-stack-overflow"></i>
                        <span class="menu-text"> Arsenal </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>



                    <ul class="submenu">

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Arsenal

                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Arsenal

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-arsenal-arsenal-general">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Geral
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-arsenal-arsenal-cme">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                CME
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                        <li class="">
                                            <a href="arsenal-arsenal-arsenal-cc">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Centro Cirurgico
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                        <li class="">
                                            <a href="arsenal-arsenal-arsenal-ass">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Assistencial
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Instrumental

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-arsenal-instrumental-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-arsenal-instrumental-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Material

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-arsenal-material-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-arsenal-material-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Containner

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-arsenal-containner-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-arsenal-containner-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Caixa

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-arsenal-box-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-arsenal-box-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="submenu">

                        <li class="">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Inventario

                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">

                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Entrada

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-inventory-in-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-inventory-in-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Saida

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-inventory-out-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-inventory-out-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Estoque

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-inventory-stock-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-inventory-stock-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="menu-icon fa fa-home orange"></i>
                                        Configurações

                                        <b class="arrow fa fa-angle-down"></b>
                                    </a>

                                    <b class="arrow"></b>

                                    <ul class="submenu">
                                        <li class="">
                                            <a href="arsenal-inventory-configuration-registration">
                                                <i class="menu-icon fa fa-bug purple"></i>
                                                Cadastro
                                            </a>

                                            <b class="arrow"></b>
                                        </li>

                                        <li class="">
                                            <a href="arsenal-inventory-configuration-search">
                                                <i class="menu-icon fa fa-file pink"></i>
                                                Consulta
                                            </a>

                                            <b class="arrow"></b>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text"> Operação </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="">
                            <a href="operation-assembly">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Montagem
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="operation-preloading">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Pre Carregamento
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="operation-loading">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Carregamento
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="operation-unloading">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Descarregamento
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="point-equipament">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Equipamento
                            </a>

                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-exchange"></i>
                        <span class="menu-text"> Movimentação </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="">
                            <a href="exchange-room">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Sala cirurgica
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="exchange-conference">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Conferencia
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="exchange-sector">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Setor Assistencial
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="exchange-manutence">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Manutenção
                            </a>

                            <b class="arrow"></b>

                    </ul>
                </li>

                <li class="">
                    <a href="calendar">
                        <i class="menu-icon fa fa-calendar"></i>

                        <span class="menu-text">
                            Calendario

                        </span>
                    </a>

                    <b class="arrow"></b>
                </li>

                <!--            <li class="">
<a href="gallery">
<i class="menu-icon fa fa-picture-o"></i>
<span class="menu-text"> Imagem </span>
</a>

<b class="arrow"></b>
</li> -->

                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-tag"></i>
                        <span class="menu-text"> Administrativo </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="">


                            <b class="arrow"></b>

                            <ul class="submenu">
                                <li class="">


                                    <b class="arrow"></b>
                                </li>
                                <li class="">




                            </ul>
                        </li>

                        <li class="">
                            <a href="adm-top-menu">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Menu Superior
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="">
                            <a href="adm-down-menu">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Menu Inferior
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="adm-workstation">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Workstation
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="adm-sensors">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Sensores
                            </a>

                            <b class="arrow"></b>
                        </li>


                </li>


            </ul>
            </li>

            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-file-o"></i>

                    <span class="menu-text">
                        Recursos

                        <span class="badge badge-primary"></span>
                    </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="resources-faq">
                            <i class="menu-icon fa fa-caret-right"></i>
                            FAQ
                        </a>
                    <li class="">
                        <a href="resources-checklist">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Check list
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-caret-right"></i>

                            Acondicionamento
                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">
                            <li class="">
                                <a href="resources-packaging-arsenal">
                                    <i class="menu-icon fa fa-fire red"></i>
                                    Arsenal
                                </a>

                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="resources-packaging-expurgo">
                                    <i class="menu-icon fa fa fa-fire red"></i>
                                    Expurgo
                                </a>

                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="resources-packaging-montagem">
                                    <i class="menu-icon fa fa fa-fire red"></i>
                                    Montagem
                                </a>

                                <b class="arrow"></b>
                            </li>
                        </ul>
                    </li>

                    <li class="">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-caret-right"></i>

                            Etiquetas
                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">
                            <li class="">
                                <a href="resources-tag-handbook">
                                    <i class="menu-icon fa fa-hand-paper-o green"></i>
                                    Manual
                                </a>

                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="resources-tag-kit">
                                    <i class="menu-icon fa fa-barcode green"></i>
                                    kit
                                </a>

                                <b class="arrow"></b>
                            </li>

                            <li class="">
                                <a href="#" class="dropdown-toggle">
                                    <i class="menu-icon fa fa-pencil orange"></i>
                                    Impressão

                                    <b class="arrow fa fa-angle-down"></b>
                                </a>

                                <b class="arrow"></b>

                                <ul class="submenu">
                                    <li class="">
                                        <a href="resources-print-cme">
                                            <i class="menu-icon fa fa-user purple"></i>
                                            CME
                                        </a>

                                        <b class="arrow"></b>
                                    </li>

                                    <li class="">
                                        <a href="resources-print-ass">
                                            <i class="menu-icon fa fa-group pink"></i>
                                            Assistencial
                                        </a>

                                        <b class="arrow"></b>
                                    </li>
                                    <li class="">
                                        <a href="resources-print-cc">
                                            <i class="menu-icon fa fa-group pink"></i>
                                            CC
                                        </a>

                                        <b class="arrow"></b>
                                    </li>
                                </ul>
                            </li>
                    </li>
                </ul>




            </li>


            <b class="arrow"></b>
            </li>
            <b class="arrow"></b>
            </li>

            <!--                <li class="">
<a href="error-404">
<i class="menu-icon fa fa-caret-right"></i>
Error 404
</a>

<b class="arrow"></b>
</li>

<li class="">
<a href="error-500">
<i class="menu-icon fa fa-caret-right"></i>
Error 500
</a>

<b class="arrow"></b>
</li>

<!--            <li class="">
<a href="grid">
<i class="menu-icon fa fa-caret-right"></i>
Grid
</a>

<b class="arrow"></b>
</li> -->

            <!--            <li class="">
<a href="blank">
<i class="menu-icon fa fa-caret-right"></i>
Blank Page
</a>

<b class="arrow"></b>
</li> -->
            </ul>
            </li>
            <li class="">
                <a href="logout">
                    <i class="menu-icon glyphicon  glyphicon-off"></i>

                    <span class="menu-text">
                        Logout


                    </span>
                </a>

                <b class="arrow"></b>
            </li>


            <!-- /menu footer buttons -->
            </ul><!-- /.nav-list -->


            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
                    data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
        </div>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                      
                    </ul><!-- /.breadcrumb -->

                    <div class="nav-search" id="nav-search">
                        <form class="form-search">

                        </form>
                    </div><!-- /.nav-search -->
                </div>

                <div class="page-content">
                    <div class="ace-settings-container" id="ace-settings-container">
                        <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                            <i class="ace-icon fa fa-cog bigger-130"></i>
                        </div>

                        <div class="ace-settings-box clearfix" id="ace-settings-box">
                            <div class="pull-left width-50">
                                <div class="ace-settings-item">
                                    <div class="pull-left">
                                        <select id="skin-colorpicker" class="hide">
                                            <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                        </select>
                                    </div>
                                    <span>&nbsp; Choose Skin</span>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                        id="ace-settings-navbar" autocomplete="off" />
                                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                        id="ace-settings-sidebar" autocomplete="off" />
                                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                        id="ace-settings-breadcrumbs" autocomplete="off" />
                                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                        autocomplete="off" />
                                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                        id="ace-settings-add-container" autocomplete="off" />
                                    <label class="lbl" for="ace-settings-add-container">
                                        Inside
                                        <b>.container</b>
                                    </label>
                                </div>
                            </div><!-- /.pull-left -->

                            <div class="pull-left width-50">
                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                        autocomplete="off" />
                                    <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                        autocomplete="off" />
                                    <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                                </div>

                                <div class="ace-settings-item">
                                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                        autocomplete="off" />
                                    <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                                </div>
                            </div><!-- /.pull-left -->
                        </div><!-- /.ace-settings-box -->
                    </div><!-- /.ace-settings-container -->

                    <div class="page-header">
                        
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS --> 
                            <?php include 'api/api.php';?>
                            <?php include 'frontend/disable-equipament-registration-frontend.php';?>


                            <div id="top-menu" class="modal aside" data-fixed="true" data-placement="top"
                                data-background="true" data-backdrop="invisible" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body container">
                                            <?php include 'api/api-tools-top.php';?>
                                        </div>
                                    </div><!-- /.modal-content -->

                                    <button class="btn btn-inverse btn-app btn-xs ace-settings-btn aside-trigger"
                                        data-target="#top-menu" data-toggle="modal" type="button">
                                        <i data-icon1="fa-chevron-down" data-icon2="fa-chevron-up"
                                            class="ace-icon fa fa-chevron-down bigger-110 icon-only"></i>
                                    </button>
                                </div><!-- /.modal-dialog -->
                            </div>

                            <div id="bottom-menu" class="modal aside" data-fixed="true" data-placement="bottom"
                                data-background="true" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body container">
                                            <?php include 'api/api-tools-down.php';?>
                                        </div>
                                    </div><!-- /.modal-content -->

                                    <button class="btn btn-yellow btn-app btn-xs ace-settings-btn aside-trigger"
                                        data-target="#bottom-menu" data-toggle="modal" type="button">
                                        <i data-icon2="fa-chevron-down" data-icon1="fa-chevron-up"
                                            class="ace-icon fa fa-chevron-up bigger-110 icon-only"></i>
                                    </button>
                                </div><!-- /.modal-dialog -->
                            </div>

                            <div id="right-menu" class="modal aside" data-body-scroll="false" data-offset="true"
                                data-placement="right" data-fixed="true" data-backdrop="false" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header no-padding">
                                            <div class="table-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">
                                                    <span class="white">&times;</span>
                                                </button>
                                                Compliance
                                            </div>
                                        </div>

                                        <?php include 'api/api-tools.php';?>
                                    </div><!-- /.modal-content -->

                                    <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn"
                                        data-target="#right-menu" data-toggle="modal" type="button">
                                        <i data-icon1="fa-plus" data-icon2="fa-minus"
                                            class="ace-icon fa fa-plus bigger-110 icon-only"></i>
                                    </button>
                                </div><!-- /.modal-dialog -->
                            </div>


                            <div id="my-modal" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                            <h3 class="smaller lighter blue no-margin">A modal with a slider in it!</h3>
                                        </div>

                                        <div class="modal-body">
                                            Some content
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            1
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            2
                                        </div>

                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
                                                <i class="ace-icon fa fa-times"></i>
                                                Close
                                            </button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>

                            <div id="aside-inside-modal" class="modal" data-placement="bottom" data-background="true"
                                data-backdrop="false" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-xs-12 white">
                                                    <h3 class="lighter no-margin">Inside another modal</h3>

                                                    <br />
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal-content -->

                                    <button class="btn btn-default btn-app btn-xs ace-settings-btn aside-trigger"
                                        data-target="#aside-inside-modal" data-toggle="modal" type="button">
                                        <i data-icon2="fa-arrow-down" data-icon1="fa-arrow-up"
                                            class="ace-icon fa fa-arrow-up bigger-110 icon-only"></i>
                                    </button>
                                </div><!-- /.modal-dialog -->
                            </div>

                            <br />


                            <!-- PAGE CONTENT ENDS -->
                            <!-- /page content -->

                            <!-- footer content -->
                            <div class="footer">

                                <div class="footer-inner">

                                    <div class="footer-content">

                                        <span class="bigger-120">

                                            <span class="blue bolder">IRIS</span>

                                            MK Sistemas Biomédicos &copy; 2021
                                        </span>

                                        &nbsp; &nbsp;
                                        <!--    <span class="action-buttons">
<a href="#">
<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
</a>

<a href="#">
<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
</a>

<a href="#">
<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
</a>
</span> -->
                                    </div>
                                </div>
                            </div>
                            <!-- /footer content -->
                        </div>
                    </div>
                    <!-- basic scripts -->

                    <!--[if !IE]> -->
                    <script src="../../../framework/assets/js/jquery-2.1.4.min.js"></script>

                    <!-- <![endif]-->

                    <!--[if IE]>
<script src="../../../framework/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
                    <script type="text/javascript">
                    if ('ontouchstart' in document.documentElement) document.write(
                        "<script src='../../../framework/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>"
                        );
                    </script>
                    <script src="../../../framework/assets/js/bootstrap.min.js"></script>

                    <!-- page specific plugin scripts -->
        <script src="../../../framework/assets/js/jquery.dataTables.min.js"></script>
		<script src="../../../framework/assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="../../../framework/assets/js/dataTables.buttons.min.js"></script>
		<script src="../../../framework/assets/js/buttons.flash.min.js"></script>
		<script src="../../../framework/assets/js/buttons.html5.min.js"></script>
		<script src="../../../framework/assets/js/buttons.print.min.js"></script>
		<script src="../../../framework/assets/js/buttons.colVis.min.js"></script>
		<script src="../../../framework/assets/js/dataTables.select.min.js"></script>
        <script src="../../../framework/assets/js/bootstrap-datepicker.min.js"></script>
		<script src="../../../framework/assets/js/jquery.jqGrid.min.js"></script>
		<script src="../../../framework/assets/js/grid.locale-en.js"></script>
        <script src="../../../framework/assets/js/jquery-ui.min.js"></script>
		<script src="../../../framework/assets/js/jquery.ui.touch-punch.min.js"></script>
        <script src="../../../framework/assets/js/jquery.nestable.min.js"></script>

      <script src="../../../framework/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../../../framework/assets/js/chosen.jquery.min.js"></script>
		<script src="../../../framework/assets/js/spinbox.min.js"></script>
		<script src="../../../framework/assets/js/bootstrap-datepicker.min.js"></script>
		<script src="../../../framework/assets/js/bootstrap-timepicker.min.js"></script>
		<script src="../../../framework/assets/js/moment.min.js"></script>
		<script src="../../../framework/assets/js/daterangepicker.min.js"></script>
		<script src="../../../framework/assets/js/bootstrap-datetimepicker.min.js"></script>
		<script src="../../../framework/assets/js/bootstrap-colorpicker.min.js"></script>
		<script src="../../../framework/assets/js/jquery.knob.min.js"></script>
		<script src="../../../framework/assets/js/autosize.min.js"></script>
		<script src="../../../framework/assets/js/jquery.inputlimiter.min.js"></script>
		<script src="../../../framework/assets/js/jquery.maskedinput.min.js"></script>
		<script src="../../../framework/assets/js/bootstrap-tag.min.js"></script>
        <script src="../../../framework/assets/js/wizard.min.js"></script>
		<script src="../../../framework/assets/js/jquery.validate.min.js"></script>
		<script src="../../../framework/assets/js/jquery-additional-methods.min.js"></script>
		<script src="../../../framework/assets/js/bootbox.js"></script>
		<script src="../../../framework/assets/js/jquery.maskedinput.min.js"></script>
		<script src="../../../framework/assets/js/select2.min.js"></script>
        
        



                    <!-- ace scripts -->
                    <script src="../../../framework/assets/js/ace-elements.min.js"></script>
                    <script src="../../../framework/assets/js/ace.min.js"></script>

                    <!-- inline scripts related to this page -->
                    <script>
                    function openCity(evt, cityName) {
                        var i, tabcontent, tablinks;
                        tabcontent = document.getElementsByClassName("tabcontent");
                        for (i = 0; i < tabcontent.length; i++) {
                            tabcontent[i].style.display = "none";
                        }
                        tablinks = document.getElementsByClassName("tablinks");
                        for (i = 0; i < tablinks.length; i++) {
                            tablinks[i].className = tablinks[i].className.replace(" active", "");
                        }
                        document.getElementById(cityName).style.display = "block";
                        evt.currentTarget.className += " active";
                    }
                    </script>
                    <script>
                    function myFunction() {
                        var x = document.getElementById("myTopnav");
                        if (x.className === "topnav") {
                            x.className += " responsive";
                        } else {
                            x.className = "topnav";
                        }
                    }
                    </script>
                    <!-- inline scripts related to this page -->
	


</body>
<script language="JavaScript">
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function() {
    history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">
// Total seconds to wait
var seconds = 1080;
var timer;
var interval;

function countdown() {

    seconds--;
    if (seconds < 0) {
        clearInterval(interval);
        // Change your redirection link here
        window.location = "https://iris.mksistemasbiomedicos.com.br";
    } else if (seconds === 60) {
        // Show warning 60 seconds before redirect
        var warning = document.createElement("div");
        warning.innerHTML =
            "O sistema será desconectado por motivos de segurança em 60 segundos. Clique aqui para continuar no sistema e reiniciar o tempo.";
        warning.style.backgroundColor = "yellow";
        warning.style.padding = "10px";
        warning.style.position = "fixed";
        warning.style.bottom = "0";
        warning.style.right = "0";
        warning.style.zIndex = "999";
        warning.addEventListener("click", function() {
            warning.style.display = "none";
            clearTimeout(seconds);
            seconds = 1080;
            countdown();
        });
        document.body.appendChild(warning);
        countdown();
    } else {
        // Update remaining seconds
        document.getElementById("countdown").innerHTML = seconds;
        // Count down using javascript

        timer = setTimeout(countdown, 1000);
    }
}

// Run countdown function
countdown();


function stopCountdown() {
    clearInterval(seconds);
}

// Reset countdown on user interaction
document.addEventListener("click", function() {
    clearTimeout(seconds);
    seconds = 1080;
    //  countdown();
});
document.addEventListener("mousemove", function() {
    seconds--;
    //clearTimeout(seconds);
    seconds = 1080;
    //  countdown();
});
</script>
<script type="text/javascript">
if ('ontouchstart' in document.documentElement) document.write(
    "<script src='../../../framework/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src=".. /../../framework/assets/js/bootstrap.min.js">
    </script>
<script type="text/javascript">
			jQuery(function($) {
			
				$( "#datepicker" ).datepicker({
					showOtherMonths: true,
					selectOtherMonths: false,
					//isRTL:true,
			
					
					/*
					changeMonth: true,
					changeYear: true,
					
					showButtonPanel: true,
					beforeShow: function() {
						//change button colors
						var datepicker = $(this).datepicker( "widget" );
						setTimeout(function(){
							var buttons = datepicker.find('.ui-datepicker-buttonpane')
							.find('button');
							buttons.eq(0).addClass('btn btn-xs');
							buttons.eq(1).addClass('btn btn-xs btn-success');
							buttons.wrapInner('<span class="bigger-110" />');
						}, 0);
					}
			*/
				});
			
			
				//override dialog's title function to allow for HTML titles
				$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
					_title: function(title) {
						var $title = this.options.title || '&nbsp;'
						if( ("title_html" in this.options) && this.options.title_html == true )
							title.html($title);
						else title.text($title);
					}
				}));
			
				$( "#id-btn-dialog1" ).on('click', function(e) {
					e.preventDefault();
			
					var dialog = $( "#dialog-message" ).removeClass('hide').dialog({
						modal: true,
						title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> jQuery UI Dialog</h4></div>",
						title_html: true,
						buttons: [ 
							{
								text: "Cancel",
								"class" : "btn btn-minier",
								click: function() {
									$( this ).dialog( "close" ); 
								} 
							},
							{
								text: "OK",
								"class" : "btn btn-primary btn-minier",
								click: function() {
									$( this ).dialog( "close" ); 
								} 
							}
						]
					});
			
					/**
					dialog.data( "uiDialog" )._title = function(title) {
						title.html( this.options.title );
					};
					**/
				});
			
			
				$( "#id-btn-dialog2" ).on('click', function(e) {
					e.preventDefault();
				
					$( "#dialog-confirm" ).removeClass('hide').dialog({
						resizable: false,
						width: '320',
						modal: true,
						title: "<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
						title_html: true,
						buttons: [
							{
								html: "<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Delete all items",
								"class" : "btn btn-danger btn-minier",
								click: function() {
									$( this ).dialog( "close" );
								}
							}
							,
							{
								html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
								"class" : "btn btn-minier",
								click: function() {
									$( this ).dialog( "close" );
								}
							}
						]
					});
				});
			
			
				
				//autocomplete
				 var availableTags = [
					"ActionScript",
					"AppleScript",
					"Asp",
					"BASIC",
					"C",
					"C++",
					"Clojure",
					"COBOL",
					"ColdFusion",
					"Erlang",
					"Fortran",
					"Groovy",
					"Haskell",
					"Java",
					"JavaScript",
					"Lisp",
					"Perl",
					"PHP",
					"Python",
					"Ruby",
					"Scala",
					"Scheme"
				];
				$( "#tags" ).autocomplete({
					source: availableTags
				});
			
				//custom autocomplete (category selection)
				$.widget( "custom.catcomplete", $.ui.autocomplete, {
					_create: function() {
						this._super();
						this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
					},
					_renderMenu: function( ul, items ) {
						var that = this,
						currentCategory = "";
						$.each( items, function( index, item ) {
							var li;
							if ( item.category != currentCategory ) {
								ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
								currentCategory = item.category;
							}
							li = that._renderItemData( ul, item );
								if ( item.category ) {
								li.attr( "aria-label", item.category + " : " + item.label );
							}
						});
					}
				});
				
				 var data = [
					{ label: "anders", category: "" },
					{ label: "andreas", category: "" },
					{ label: "antal", category: "" },
					{ label: "annhhx10", category: "Products" },
					{ label: "annk K12", category: "Products" },
					{ label: "annttop C13", category: "Products" },
					{ label: "anders andersson", category: "People" },
					{ label: "andreas andersson", category: "People" },
					{ label: "andreas johnson", category: "People" }
				];
				$( "#search" ).catcomplete({
					delay: 0,
					source: data
				});
				
				
				//tooltips
				$( "#show-option" ).tooltip({
					show: {
						effect: "slideDown",
						delay: 250
					}
				});
			
				$( "#hide-option" ).tooltip({
					hide: {
						effect: "explode",
						delay: 250
					}
				});
			
				$( "#open-event" ).tooltip({
					show: null,
					position: {
						my: "left top",
						at: "left bottom"
					},
					open: function( event, ui ) {
						ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
					}
				});
			
			
				//Menu
				$( "#menu" ).menu();
			
			
				//spinner
				var spinner = $( "#spinner" ).spinner({
					create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					}
				});
			
				//slider example
				$( "#slider" ).slider({
					range: true,
					min: 0,
					max: 500,
					values: [ 75, 300 ]
				});
			
			
			
				//jquery accordion
				$( "#accordion" ).accordion({
					collapsible: true ,
					heightStyle: "content",
					animate: 250,
					header: ".accordion-header"
				}).sortable({
					axis: "y",
					handle: ".accordion-header",
					stop: function( event, ui ) {
						// IE doesn't register the blur when sorting
						// so trigger focusout handlers to remove .ui-state-focus
						ui.item.children( ".accordion-header" ).triggerHandler( "focusout" );
					}
				});
				//jquery tabs
				$( "#tabs" ).tabs();
				
				
				//progressbar
				$( "#progressbar" ).progressbar({
					value: 37,
					create: function( event, ui ) {
						$(this).addClass('progress progress-striped active')
							   .children(0).addClass('progress-bar progress-bar-success');
					}
				});
			
				
				//selectmenu
				 $( "#number" ).css('width', '200px')
				.selectmenu({ position: { my : "left bottom", at: "left top" } })
					
			});
		</script>
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter25836836 = new Ya.Metrika({id:25836836,
					webvisor:true,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/25836836" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-38894584-2', 'auto');
	ga('send', 'pageview');

</script>
   

    <!-- ace scripts -->
    <script src=".. /../../framework/assets/js/ace-elements.min.js">
        </script>
        <script src=".. /../../framework/assets/js/ace.min.js">
            </script>

            <!-- inline scripts related to this page -->
            <script type="text/javascript">
            jQuery(function($) {
                $('.modal.aside').ace_aside();

                $('#aside-inside-modal').addClass('aside').ace_aside({
                    container: '#my-modal > .modal-dialog'
                });

                //$('#top-menu').modal('show')

                $(document).one('ajaxloadstart.page', function(e) {
                    //in ajax mode, remove before leaving page
                    $('.modal.aside').remove();
                    $(window).off('.aside')
                });


                //make content sliders resizable using jQuery UI (you should include jquery ui files)
                //$('#right-menu > .modal-dialog').resizable({handles: "w", grid: [ 20, 0 ], minWidth: 200, maxWidth: 600});
            })
            </script>
            <!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($){
			    var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
				var container1 = demo1.bootstrapDualListbox('getContainer');
				container1.find('.btn').addClass('btn-white btn-info btn-bold');
			
				/**var setRatingColors = function() {
					$(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
					$(this).find('.star-off-png').removeClass('orange2').addClass('grey');
				}*/
				$('.rating').raty({
					'cancel' : true,
					'half': true,
					'starType' : 'i'
					/**,
					
					'click': function() {
						setRatingColors.call(this);
					},
					'mouseover': function() {
						setRatingColors.call(this);
					},
					'mouseout': function() {
						setRatingColors.call(this);
					}*/
				})//.find('i:not(.star-raty)').addClass('grey');
				
				
				
				//////////////////
				//select2
				$('.select2').css('width','200px').select2({allowClear:true})
				$('#select2-multiple-style .btn').on('click', function(e){
					var target = $(this).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('.select2').addClass('tag-input-style');
					 else $('.select2').removeClass('tag-input-style');
				});
				
				//////////////////
				$('.multiselect').multiselect({
				 enableFiltering: true,
				 enableHTML: true,
				 buttonClass: 'btn btn-white btn-primary',
				 templates: {
					button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
					ul: '<ul class="multiselect-container dropdown-menu"></ul>',
					filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
					filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
					li: '<li><a tabindex="0"><label></label></a></li>',
			        divider: '<li class="multiselect-item divider"></li>',
			        liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
				 }
				});
			
				
				///////////////////
					
				//typeahead.js
				//example taken from plugin's page at: https://twitter.github.io/typeahead.js/examples/
				var substringMatcher = function(strs) {
					return function findMatches(q, cb) {
						var matches, substringRegex;
					 
						// an array that will be populated with substring matches
						matches = [];
					 
						// regex used to determine if a string contains the substring `q`
						substrRegex = new RegExp(q, 'i');
					 
						// iterate through the pool of strings and for any string that
						// contains the substring `q`, add it to the `matches` array
						$.each(strs, function(i, str) {
							if (substrRegex.test(str)) {
								// the typeahead jQuery plugin expects suggestions to a
								// JavaScript object, refer to typeahead docs for more info
								matches.push({ value: str });
							}
						});
			
						cb(matches);
					}
				 }
			
				 $('input.typeahead').typeahead({
					hint: true,
					highlight: true,
					minLength: 1
				 }, {
					name: 'states',
					displayKey: 'value',
					source: substringMatcher(ace.vars['US_STATES']),
					limit: 10
				 });
					
					
				///////////////
				
				
				//in ajax mode, remove remaining elements before leaving page
				$(document).one('ajaxloadstart.page', function(e) {
					$('[class*=select2]').remove();
					$('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
					$('.rating').raty('destroy');
					$('.multiselect').multiselect('destroy');
				});
			
			});
		</script>
        <script type="text/javascript">
			jQuery(function($) {
			
				$('[data-rel=tooltip]').tooltip();
			
				$('.select2').css('width','200px').select2({allowClear:true})
				.on('change', function(){
					$(this).closest('form').validate().element($(this));
				}); 
			
			
				var $validation = false;
				$('#fuelux-wizard-container')
				.ace_wizard({
					//step: 2 //optional argument. wizard will jump to step "2" at first
					//buttons: '.wizard-actions:eq(0)'
				})
				.on('actionclicked.fu.wizard' , function(e, info){
					if(info.step == 1 && $validation) {
						if(!$('#validation-form').valid()) e.preventDefault();
					}
				})
				//.on('changed.fu.wizard', function() {
				//})
				.on('finished.fu.wizard', function(e) {
					bootbox.dialog({
						message: "Thank you! Your information was successfully saved!", 
						buttons: {
							"success" : {
								"label" : "OK",
								"className" : "btn-sm btn-primary"
							}
						}
					});
				}).on('stepclick.fu.wizard', function(e){
					//e.preventDefault();//this will prevent clicking and selecting steps
				});
			
			
				//jump to a step
				/**
				var wizard = $('#fuelux-wizard-container').data('fu.wizard')
				wizard.currentStep = 3;
				wizard.setState();
				*/
			
				//determine selected step
				//wizard.selectedItem().step
			
			
			
				//hide or show the other form which requires validation
				//this is for demo only, you usullay want just one form in your application
				$('#skip-validation').removeAttr('checked').on('click', function(){
					$validation = this.checked;
					if(this.checked) {
						$('#sample-form').hide();
						$('#validation-form').removeClass('hide');
					}
					else {
						$('#validation-form').addClass('hide');
						$('#sample-form').show();
					}
				})
			
			
			
				//documentation : http://docs.jquery.com/Plugins/Validation/validate
			
			
				$.mask.definitions['~']='[+-]';
				$('#phone').mask('(999) 999-9999');
			
				jQuery.validator.addMethod("phone", function (value, element) {
					return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
				}, "Enter a valid phone number.");
			
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					ignore: "",
					rules: {
						email: {
							required: true,
							email:true
						},
						password: {
							required: true,
							minlength: 5
						},
						password2: {
							required: true,
							minlength: 5,
							equalTo: "#password"
						},
						name: {
							required: true
						},
						phone: {
							required: true,
							phone: 'required'
						},
						url: {
							required: true,
							url: true
						},
						comment: {
							required: true
						},
						state: {
							required: true
						},
						platform: {
							required: true
						},
						subscription: {
							required: true
						},
						gender: {
							required: true,
						},
						agree: {
							required: true,
						}
					},
			
					messages: {
						email: {
							required: "Please provide a valid email.",
							email: "Please provide a valid email."
						},
						password: {
							required: "Please specify a password.",
							minlength: "Please specify a secure password."
						},
						state: "Please choose state",
						subscription: "Please choose at least one option",
						gender: "Please choose gender",
						agree: "Please accept our policy"
					},
			
			
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
					},
					invalidHandler: function (form) {
					}
				});
			
				
				
				
				$('#modal-wizard-container').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
				
				
				/**
				$('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				
				$('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				*/
				
				
				$(document).one('ajaxloadstart.page', function(e) {
					//in ajax mode, remove remaining elements before leaving page
					$('[class*=select2]').remove();
				});
			})
		</script>
        <!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($){
	
	$('textarea[data-provide="markdown"]').each(function(){
        var $this = $(this);

		if ($this.data('markdown')) {
		  $this.data('markdown').showEditor();
		}
		else $this.markdown()
		
		$this.parent().find('.btn').addClass('btn-white');
    })
	
	
	
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			//console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	}

	//$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

	//but we want to change a few buttons colors for the third style
	$('#editor1').ace_wysiwyg({
		toolbar:
		[
			'font',
			null,
			'fontSize',
			null,
			{name:'bold', className:'btn-info'},
			{name:'italic', className:'btn-info'},
			{name:'strikethrough', className:'btn-info'},
			{name:'underline', className:'btn-info'},
			null,
			{name:'insertunorderedlist', className:'btn-success'},
			{name:'insertorderedlist', className:'btn-success'},
			{name:'outdent', className:'btn-purple'},
			{name:'indent', className:'btn-purple'},
			null,
			{name:'justifyleft', className:'btn-primary'},
			{name:'justifycenter', className:'btn-primary'},
			{name:'justifyright', className:'btn-primary'},
			{name:'justifyfull', className:'btn-inverse'},
			null,
			{name:'createLink', className:'btn-pink'},
			{name:'unlink', className:'btn-pink'},
			null,
			{name:'insertImage', className:'btn-success'},
			null,
			'foreColor',
			null,
			{name:'undo', className:'btn-grey'},
			{name:'redo', className:'btn-grey'}
		],
		'wysiwyg': {
			fileUploadError: showErrorAlert
		}
	}).prev().addClass('wysiwyg-style2');

	
	/**
	//make the editor have all the available height
	$(window).on('resize.editor', function() {
		var offset = $('#editor1').parent().offset();
		var winHeight =  $(this).height();
		
		$('#editor1').css({'height':winHeight - offset.top - 10, 'max-height': 'none'});
	}).triggerHandler('resize.editor');
	*/
	

	$('#editor2').css({'height':'200px'}).ace_wysiwyg({
		toolbar_place: function(toolbar) {
			return $(this).closest('.widget-box')
			       .find('.widget-header').prepend(toolbar)
				   .find('.wysiwyg-toolbar').addClass('inline');
		},
		toolbar:
		[
			'bold',
			{name:'italic' , title:'Change Title!', icon: 'ace-icon fa fa-leaf'},
			'strikethrough',
			null,
			'insertunorderedlist',
			'insertorderedlist',
			null,
			'justifyleft',
			'justifycenter',
			'justifyright'
		],
		speech_button: false
	});
	
	


	$('[data-toggle="buttons"] .btn').on('click', function(e){
		var target = $(this).find('input[type=radio]');
		var which = parseInt(target.val());
		var toolbar = $('#editor1').prev().get(0);
		if(which >= 1 && which <= 4) {
			toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
			if(which == 1) $(toolbar).addClass('wysiwyg-style1');
			else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
			if(which == 4) {
				$(toolbar).find('.btn-group > .btn').addClass('btn-white btn-round');
			} else $(toolbar).find('.btn-group > .btn-white').removeClass('btn-white btn-round');
		}
	});


	

	//RESIZE IMAGE
	
	//Add Image Resize Functionality to Chrome and Safari
	//webkit browsers don't have image resize functionality when content is editable
	//so let's add something using jQuery UI resizable
	//another option would be opening a dialog for user to enter dimensions.
	if ( typeof jQuery.ui !== 'undefined' && ace.vars['webkit'] ) {
		
		var lastResizableImg = null;
		function destroyResizable() {
			if(lastResizableImg == null) return;
			lastResizableImg.resizable( "destroy" );
			lastResizableImg.removeData('resizable');
			lastResizableImg = null;
		}

		var enableImageResize = function() {
			$('.wysiwyg-editor')
			.on('mousedown', function(e) {
				var target = $(e.target);
				if( e.target instanceof HTMLImageElement ) {
					if( !target.data('resizable') ) {
						target.resizable({
							aspectRatio: e.target.width / e.target.height,
						});
						target.data('resizable', true);
						
						if( lastResizableImg != null ) {
							//disable previous resizable image
							lastResizableImg.resizable( "destroy" );
							lastResizableImg.removeData('resizable');
						}
						lastResizableImg = target;
					}
				}
			})
			.on('click', function(e) {
				if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
					destroyResizable();
				}
			})
			.on('keydown', function() {
				destroyResizable();
			});
	    }

		enableImageResize();

		/**
		//or we can load the jQuery UI dynamically only if needed
		if (typeof jQuery.ui !== 'undefined') enableImageResize();
		else {//load jQuery UI if not loaded
			//in Ace demo ./components will be replaced by correct components path
			$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
				enableImageResize()
			});
		}
		*/
	}


});
		</script>

 


</html>