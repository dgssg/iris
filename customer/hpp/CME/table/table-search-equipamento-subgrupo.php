<?php
include("../database/database.php");
$query = "SELECT equipamento_subgrupo.id,equipamento_grupo.nome AS 'grupo',equipamento_subgrupo.nome,equipamento_subgrupo.codigo,equipamento_subgrupo.fabricante,equipamento_subgrupo.modelo from equipamento_subgrupo INNER JOIN equipamento_grupo ON equipamento_grupo.id = equipamento_subgrupo.id_equipamento_grupo where equipamento_subgrupo.trash =1 order by equipamento_subgrupo.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
