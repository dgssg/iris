<?php
include("../database/database.php");
$query = "SELECT caixa_subgrupo.id,caixa_grupo.nome AS 'grupo',caixa_subgrupo.nome,caixa_subgrupo.codigo,caixa_subgrupo.fabricante,caixa_subgrupo.modelo from caixa_subgrupo INNER JOIN caixa_grupo ON caixa_grupo.id = caixa_subgrupo.id_caixa_grupo where caixa_subgrupo.trash =1 order by caixa_subgrupo.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
