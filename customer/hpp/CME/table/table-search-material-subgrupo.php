<?php
include("../database/database.php");
$query = "SELECT material_subgrupo.id,material_grupo.nome AS 'grupo',material_subgrupo.nome,material_subgrupo.codigo,material_subgrupo.fabricante,material_subgrupo.modelo from material_subgrupo INNER JOIN material_grupo ON material_grupo.id = material_subgrupo.id_material_grupo where material_subgrupo.trash =1 order by material_subgrupo.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
