<?php
include("../database/database.php");
$query = "SELECT catalogo_subgrupo.id,catalogo_grupo.nome AS 'grupo',catalogo_subgrupo.nome,catalogo_subgrupo.codigo from catalogo_subgrupo INNER JOIN catalogo_grupo ON catalogo_grupo.id = catalogo_subgrupo.id_catalogo_grupo where catalogo_subgrupo.trash =1 order by catalogo_subgrupo.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
