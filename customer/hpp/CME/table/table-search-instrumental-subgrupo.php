<?php
include("../database/database.php");
$query = "SELECT instrumental_subgrupo.id,instrumental_grupo.nome AS 'grupo',instrumental_subgrupo.nome,instrumental_subgrupo.codigo,instrumental_subgrupo.fabricante,instrumental_subgrupo.modelo from instrumental_subgrupo INNER JOIN instrumental_grupo ON instrumental_grupo.id = instrumental_subgrupo.id_instrumental_grupo where instrumental_subgrupo.trash =1 order by instrumental_subgrupo.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
