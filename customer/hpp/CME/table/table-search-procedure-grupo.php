<?php
include("../database/database.php");
$query = "SELECT id,nome from procedimento_grupo order by procedimento_grupo.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
