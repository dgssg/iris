<?php
include("../database/database.php");


$sql = "SELECT catalogo.id, catalogo_subgrupo.nome, catalogo.codigo,catalogo.material,catalogo.familia FROM catalogo  INNER JOIN catalogo_subgrupo ON catalogo_subgrupo.id = catalogo.id_catalogo_subgrupo ORDER BY id DESC";

$result = $conn->query($sql);

$groups = array();
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $groups[] = array(
      'id' => $row['id'],
      'nome' => $row['nome'],
      'codigo' => $row['codigo'],
      'material' => $row['material'],
      'familia' => $row['familia']
  
    );
  }
}

// retorna as informações dos grupos em formato JSON
header('Content-Type: application/json');
echo json_encode($groups);

// fecha a conexão com o banco de dados
$conn->close();
?>