<?php
include("../database/database.php");

$sql = "SELECT id, nome, codigo FROM material_grupo ORDER BY id DESC";

$result = $conn->query($sql);

$groups = array();
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $groups[] = array(
      'id' => $row['id'],
      'nome' => $row['nome'],
      'codigo' => $row['codigo']
    );
  }
}

// retorna as informações dos grupos em formato JSON
header('Content-Type: application/json');
echo json_encode($groups);

// fecha a conexão com o banco de dados
$conn->close();
?>