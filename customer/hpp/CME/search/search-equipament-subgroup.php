<?php
include("../database/database.php");
$codigoget = ($_GET["id"]);

$sql = "SELECT id, nome, codigo,fabricante,modelo FROM equipamento_subgrupo where id_equipamento_grupo =  $codigoget ORDER BY id DESC";

$result = $conn->query($sql);

$groups = array();
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $groups[] = array(
      'id' => $row['id'],
      'nome' => $row['nome'],
      'codigo' => $row['codigo'],
      'fabricante' => $row['fabricante'],
      'modelo' => $row['modelo']
    );
  }
}

// retorna as informações dos grupos em formato JSON
header('Content-Type: application/json');
echo json_encode($groups);

// fecha a conexão com o banco de dados
$conn->close();
?>