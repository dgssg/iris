var DEF_WASM_PATH = '/js/datasymbol-sdk.wasm';

var DEF_BARCODE_TYPE = ['Code128', 'Code39', 'EAN13', 'UPCA', 'DataMatrix', 'QRCode', 'QRCodeUnrecognized'];

var camDevices=null;
var barNum = 0;

window.onload = function ()
{
	$("div#ScanBarcodesBtn").css( "visibility", "visible" );
	$("div#ScanBarcodesBtn").addClass("wobble");

	DSScanner.addEventListener('onError', onError);

	DSScanner.getVideoDevices(function (devices) {
		devices.forEach(function (device) {
			console.log("device:" + device.label + '|' + device.id);
		});
		if(devices.length > 0)
		{
			camDevices = devices.slice();
			CreateScanner(devices[0]);
		}
		else
		{
			onError( {name: 'NotFoundError', message: ''} );
		}
	});

	//scan barcodes click
	$('#ScanBarcodesBtn').on('click', function() {
		ScanBarcodes(this);
	});

	//barcode settings click
	$('#ShowBarcodeSettings').on('click', function() {
		ShowBarcodeSettings();
	});

	//scanner settings click
	$('#ShowScannerSettings').on('click', function() {
		ShowScannerSettings();
	});

	//examples click
	$('#ShowWebSDKExamples').on('click', function() {
		ShowWebSDKExamples();
	});

	//ESC and hide popup
	$(document).keyup(function(e) {
		if (e.keyCode == 27 || e.keyCode == 13) {
			HidePopup(e);
		}
	});

	//listen the mouseup to hide popup
	$(document).on('touchend mouseup', function(e) {
		/*e.stopPropagation(); // Stop event bubbling.
		e.preventDefault(); // Prevent default behaviour
		if (e.type === 'touchend') $(this).off('click'); // If event type was touch turn off clicks to prevent phantom clicks*/
		HidePopup(e);
	});

	$("div#ScanBarcodesBtn").hover(function(){
	    $(this).css("background-color", "#889");
	    }, function(){
	    $(this).css("background-color", "#404050");
	});

       	SetErrorStatus("Loading ...", "", false);
}

function HidePopup(e) {
	var popup = $('div.popupwnd:visible');
	if(!popup || !popup.length) return;

	// if the target of the click isn't the container nor a descendant of the container
	if( e == null || (!popup.is(e.target) && popup.has(e.target).length === 0) )
	{
		popup.fadeOut(1000);
		//popup.hide();
		popup.css({opacity:0.1});

		if( popup.attr('id') == 'WebSDKExamples' )
			return;

		DSScanner.StopScanner();

		if(popup.attr('id') == 'BarcodeSettings')
			OnCloseBarcodeSettings();
		else if(popup.attr('id') == 'ScannerSettings')
			OnCloseScannerSettings();
	}
}

function onBarcodeReady(barcodeResult)
{
	const arrBarcodes = ['6945040100638', '027242872448', '027242882362', '6954176835680'];
	const arrBarcodesTitle = ['Contec. Pulse Oximeter.', 'Sony. E50 F1.8 Lens.', 'Sony. ILCE-5100 Digital Camera', 'Mi6. Mobile Phone.'];

	var bRed = false;
	for (var i = 0; i < barcodeResult.length; ++i) {
		if(barcodeResult[i].type == 'LinearUnrecognized' || barcodeResult[i].type == 'QRCodeUnrecognized' || barcodeResult[i].type == 'DataMatrixUnrecognized' ||
			barcodeResult[i].type == 'PDF417Unrecognized' || barcodeResult[i].type == 'AztecUnrecognized' ||
			!barcodeResult[i].barcodeAtPoint )
		{
			bRed = true;
			continue;
		}

	        var sBarcode = DSScanner.bin2String(barcodeResult[i]);

		if( barNum > 7 )
			$("div#invtable div.row:first").remove();

		var _sBarcode = arrBarcodes[barNum % 4];
		var sBarcodeTitle = arrBarcodesTitle[barNum % 4];
		$("div#invtable").append( "<div class='row'><div class='cell cell1'><img src='/app1m/barcode_images/"+ _sBarcode +".jpg' width=120 border=0></div><div class='cell'>"+ sBarcode +"<br>"+ sBarcodeTitle +"</div></div>" );
		//$("div#invtable").append( "<div class='row'><div class='cell cell1'>" + barNum + "</div><div class='cell'>"+ sBarcode +"</div></div>" );
		barNum++;
	}

	if( bRed && barcodeResult.length == 1 )
		return;

	$("div#ScanBarcodesBtn").css( "background-color", "#070" );
	setTimeout( function () {
		$("div#ScanBarcodesBtn").css( "background-color", "#404050" );
	}, 300 );
}


function onError(err) {
	console.error(err);
	SetErrorStatus("", "", false);

	var unsupportedMsg = 'Please use any of the following browsers<br><br><table style="border-collapse:separate;border-spacing:20px;"><tr><td><b>Desktop:</b></td><td><b>Mobile Android:</b></td><td><b>Mobile iOS:</b></td></tr><tr><td>Chrome 57+<br>Firefox 52+<br>Opera 44+<br>Safari 11+<br>Edge 16+</td><td valign="top">Chrome 59+<br>Firefox 55+</td><td valign="top">Safari 11+</td></tr></table>';

	if(err.name == 'NotFoundError')
        	SetErrorStatus("No Camera", "");
	else if(err.name == 'PermissionDeniedError')
        	SetErrorStatus("Permission Denied", "");
	else if(err.name == 'NotAllowedError')
        	SetErrorStatus("Not Allowed", err.message);
	else if(err.name == 'NotCompatibleBrowser' || err.name == 'ModuleAbort' || err.name == 'CannotEnumDevices')
        	SetErrorStatus("Unsupported Browser", unsupportedMsg);
	else if(err.name == 'CannotInitLib')
        	SetErrorStatus("WASM Library Error", "Cannot initialize barcode decoder SDK");
	else
	{
        	SetErrorStatus("Error", err.message);
	}
}

function CreateScanner(device){
	var bMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	var scannerSettings = {
		scanner: {
			key: 'B053EsVoNC4TT98dgSmmLkDBGFYe4UuRrqcYiyAVwuZ++Wgzeahz4r9TczQhPy/vB1Rx6iLa/ar6NSPSgZqOj4I4UE1Ybsg/GnNcf+1uEaDOK4OmW81E/fkrAIwwYcQKAR2CjuD4TSK3Q5BQPKn+gce/8ZEZUO3WjXUk68TFHQJzpB0LW2Sw0iSezprt7jU8wpZcdDesXkaYvS4p2zIOZqGhSPjnE4yJoX8Kx1W9QY+hzNZRj3jCdBgQOVANb2boB3DSpFKE4M/utaBctP/Q6ZXVGhiipWHqBjQ9byjLLssAauz45b5xTAKJYdB+sxe/C2/NnVRabq26iIUbdlkoAA==',
			//beepData: 'https://websdk.datasymbol.com/audio/beep1.mp3',
			barcodeAtPoint: true,
		},
		viewport: {
			id: 'datasymbol-barcode-viewport',
			width: null,	//null means 100% width
			//width: bMobile ? null : 640,	//null means 100% width
			//height: 200,
		},
		camera: {
			resx: 640,
			resy: 480,
		},
		barcode: {
			barcodeTypes: DEF_BARCODE_TYPE,
		},
	};

	if(bMobile)
	{
		scannerSettings.camera.facingMode = 'environment';
	}
	else
	{
		scannerSettings.camera.id = device ? device.id : null;
		scannerSettings.camera.label = device ? device.label : null;
	}

	//DSScanner.addEventListener('onError', onError);

	DSScanner.addEventListener('onBarcode', onBarcodeReady);

	DSScanner.addEventListener('onScannerReady', function () {
		console.log('HTML onScannerReady');
		SetErrorStatus("", "", false);
		//DSScanner.StartScanner();
	});

	DSScanner.Create(scannerSettings);
}

function SetErrorStatus(errTitle, errMsg, bShowImg=true) {
	var errMessage = "<center>";
	if(bShowImg)
		errMessage += "<img src='/images/warning_64.png' border=0></br>";
	errMessage += "<span class='v2'>"+errTitle+"</span></center></br>";
	errMessage += errMsg;

	var statusElement = document.getElementById('status');
	statusElement.innerHTML = errMessage;
}

function ShowPopup(buttonId, popupId)
{
	var jButton = $('#'+buttonId);

	var jDivPopupWnd = $('#' + popupId);

	//attach popup to the menubar
	if( popupId == 'ScanBarcodesDiv' )
		var jLoginDiv = $('.divbarlogin');
	else
		var jLoginDiv = $('.menubar');
	jDivPopupWnd.detach();
	jLoginDiv.append(jDivPopupWnd);

	var _top = jLoginDiv.outerHeight() + 3;
	if( popupId == 'WebSDKExamples')
		jDivPopupWnd.css({left: jButton.position().left+jButton.outerWidth()-jDivPopupWnd.outerWidth(), top: _top-30, display:'inline-block'});
	else
		jDivPopupWnd.css({left: -1, top: _top-30, display:'inline-block'});
	if( popupId == 'ScanBarcodesDiv' )
		jDivPopupWnd.outerWidth( jLoginDiv.outerWidth() );
	jDivPopupWnd.animate({top: _top, opacity: 1}, 300);
}

function ScanBarcodes(el)
{
	if( $("div#ScanBarcodesDiv").is(":visible") )
		return;

	ShowPopup('ScanBarcodesBtn', 'ScanBarcodesDiv');

	//if scanner ready, start scanning
	if( DSScanner.IsScannerReady() )
	{
		DSScanner.StartScanner();
	}
	//try latter (3s)
	else
	{
		setTimeout( function () {
			DSScanner.StartScanner();
		}, 3000 );
	}
}


function ShowBarcodeSettings()
{
	var _ScannerSettings = DSScanner.getScannerSettings();
	var jBarcodeSettingsWnd = $('#BarcodeSettings');

        for (var i = 0; i < _ScannerSettings.barcode.barcodeTypes.length; ++i)
	{
		var barCheck = jBarcodeSettingsWnd.find('#' + _ScannerSettings.barcode.barcodeTypes[i]);
		if( barCheck.size() )
			barCheck.prop('checked', true);
	}

	jBarcodeSettingsWnd.find('#uiLinearFindBarcodes').val(_ScannerSettings.barcode.uiLinearFindBarcodes);
	jBarcodeSettingsWnd.find('#uiPDF417FindBarcodes').val(_ScannerSettings.barcode.uiPDF417FindBarcodes);
	jBarcodeSettingsWnd.find('#uiQRCodeFindBarcodes').val(_ScannerSettings.barcode.uiQRCodeFindBarcodes);
	jBarcodeSettingsWnd.find('#uiDataMatrixFindBarcodes').val(_ScannerSettings.barcode.uiDataMatrixFindBarcodes);
	jBarcodeSettingsWnd.find('#uiAztecCodeFindBarcodes').val(_ScannerSettings.barcode.uiAztecCodeFindBarcodes);

	ShowPopup('ShowBarcodeSettings', 'BarcodeSettings');
}

function ShowScannerSettings()
{
	var _ScannerSettings = DSScanner.getScannerSettings();
	var jScannerSettingsWnd = $('#ScannerSettings');

	//add cameras
	var jSelect = jScannerSettingsWnd.find('#camdevices').eq(0);
	jSelect.children('option').remove();

	if( camDevices )
	{
		camDevices.forEach(function (device) {
			jSelect.append($('<option>', {
			    value: device.id,
			    text: device.label,
			}));
			if(_ScannerSettings.camera.id == device.id)
				jSelect.children().last().prop( "selected", true );
		});
	}

	jScannerSettingsWnd.find('label.switch input[type=checkbox]#beep').eq(0).prop('checked', _ScannerSettings.scanner.beep);
	jScannerSettingsWnd.find('label.switch input[type=checkbox]#barcodeatpoint').eq(0).prop('checked', _ScannerSettings.scanner.barcodeAtPoint);

	jScannerSettingsWnd.find('#frameTimeout').val(_ScannerSettings.scanner.frameTimeout);
	jScannerSettingsWnd.find('#barcodeTimeout').val(_ScannerSettings.scanner.barcodeTimeout);

	ShowPopup('ShowScannerSettings', 'ScannerSettings');
}

function ShowWebSDKExamples()
{
	ShowPopup('ShowWebSDKExamples', 'WebSDKExamples');
}

function OnCloseBarcodeSettings()
{
	var scannerSettings = {
		barcode:{ barcodeTypes: [] }
	};

	//collect checked barcode types
	//var barcode = { barcodeTypes: [] }
	var jBarcodeSettingsWnd = $('#BarcodeSettings');

	var barChecks = jBarcodeSettingsWnd.find('label.switch input[type=checkbox]');

        for (var i = 0; i < barChecks.size(); ++i)
	{
		var jCheckBox = barChecks.eq(i);
		var id = jCheckBox.attr('id');
		var bChecked = jCheckBox.prop('checked');

		if( id == 'pdf417micro' )
			scannerSettings.barcode.bPDF417FindMicro = bChecked;
		else if( id == 'qrmicro' )
			scannerSettings.barcode.bQRCodeFindMicro = bChecked;
		else if( bChecked )
			scannerSettings.barcode.barcodeTypes.push( barChecks.eq(i).attr('id') );
	}

	var barTexts = jBarcodeSettingsWnd.find('input[type=text]');
        for (var i = 0; i < barTexts.size(); ++i)
	{
		var jText = barTexts.eq(i);
		var val = parseInt(jText.val());
		if(val != NaN)
			scannerSettings.barcode[jText.attr('id')] = val;
	}
	//console.log(scannerSettings);
	DSScanner.setScannerSettings( scannerSettings );
}

function OnCloseScannerSettings()
{
	var scannerSettings = {
		scanner: {
		},
	};


	var jScannerSettingsWnd = $('#ScannerSettings');

	var texts = jScannerSettingsWnd.find('input[type=text]');
        for (var i = 0; i < texts.size(); ++i)
	{
		var jText = texts.eq(i);
		var val = parseInt(jText.val());
		if(val != NaN)
			ScannerSettings[jText.attr('id')] = scannerSettings.scanner[jText.attr('id')] = val;
	}

	scannerSettings.scanner.beep = jScannerSettingsWnd.find('label.switch input[type=checkbox]#beep').prop('checked');
	scannerSettings.scanner.barcodeAtPoint = jScannerSettingsWnd.find('label.switch input[type=checkbox]#barcodeatpoint').prop('checked');

	var jOptionSelected = jScannerSettingsWnd.find('#camdevices option:selected').eq(0);
	var selCamId = jOptionSelected.val();
	if(selCamId != DSScanner.getScannerSettings().camera.id)
	{
		scannerSettings.camera = {};
		scannerSettings.camera.id = selCamId;
		scannerSettings.camera.label = jOptionSelected.text();
	}

	console.log('SetScannerSettings');

	DSScanner.setScannerSettings(scannerSettings);
}
